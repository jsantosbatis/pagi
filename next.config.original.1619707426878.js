module.exports = {
  target: 'serverless',
  webpack: (
    config,
    { _buildId, _dev, _isServer, _defaultLoaders, _webpack },
  ) => {
    config.externals.push('sharp');
    return config;
  },
};
