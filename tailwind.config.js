const colors = require('tailwindcss/colors');

module.exports = {
  purge: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'cool-gray': colors.coolGray,
        yellow: colors.amber,
        'light-blue': colors.lightBlue,
        teal: colors.teal,
        primary: colors.lightBlue,
        secondary: colors.teal,
      },
    },
  },
  variants: {
    extend: {
      cursor: ['disabled'],
      opacity: ['disabled'],
      visibility: ['disabled'],
    },
  },
  plugins: [require('@tailwindcss/forms')],
};
