import axios from 'axios';

type Event = {
  version: string;
  id: string;
  'detail-type': string;
  source: string;
  account: string;
  time: Date;
  region: string;
  ressources: [];
  detail: any;
};
export default async (event: Event): Promise<void> => {
  try {
    const response = await axios.post(event.detail.url, {
      ...event.detail.transaction,
    });
    console.log(event, response.status, response.statusText);
    if (response.status !== 200 && response.status !== 201) {
      console.log(response.data);
      throw new Error(`Call ${event.detail.url} to was not successful`);
    }
  } catch (error: any) {
    console.error(error);
    throw new Error(error.message);
  }
};
