import axios from 'axios';

export default async (event): Promise<void> => {
  console.log(event);
  await axios.post(`${process.env.PIUNIFY_URL}/api/createUser`, {
    ...event,
  });
  return event;
};
