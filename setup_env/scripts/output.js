/* eslint-disable no-console */
const { default: axios } = require('axios');

function handler(data, serverless, options) {
  const region = serverless.configurationInput.provider.region;
  console.log(
    `https://cognito-idp.${region}.amazonaws.com/${data.UserPoolId}/.well-known/jwks.json`,
  );
  axios
    .get(
      `https://cognito-idp.${region}.amazonaws.com/${data.UserPoolId}/.well-known/jwks.json`,
    )
    .then((res) => {
      console.log(res.data);
    });
  console.log(`PAGI_TABLE="${data.TableName}"`);
  console.log(`PAGI_AWS_REGION="${region}"`);
  console.log(`PAGI_AWS_USER_POOL_ID="${data.UserPoolId}"`);
  console.log(`PAGI_AWS_APP_CLIENT_ID="${data.AppClientId}"`);
  console.log(`PAGI_AWS_IDENTITY_POOL_ID="${data.IdentityPoolId}"`);
  console.log(`PAGI_AWS_BUCKET="${data.BucketName}"`);
  console.log(`NEXT_PUBLIC_PAGI_AWS_REGION="${region}"`);
  console.log(`NEXT_PUBLIC_PAGI_AWS_USER_POOL_ID="${data.UserPoolId}"`);
  console.log(`NEXT_PUBLIC_PAGI_AWS_APP_CLIENT_ID="${data.AppClientId}"`);
  console.log(`NEXT_PUBLIC_PAGI_AWS_IDENTITY_POOL_ID="${data.IdentityPoolId}"`);
  console.log(`NEXT_PUBLIC_PAGI_AWS_BUCKET="${data.BucketName}"`);
  console.log('Received Stack Output', data);
}

module.exports = { handler };
