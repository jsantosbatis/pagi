import { NextApiRequest, NextApiResponse } from 'next';
import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import getTransactionByProvider from 'src/graphql/transaction/helpers/getTransactionByProvider';
import updateTransaction from 'src/graphql/transaction/helpers/updateTransaction';
import eventBridge from '~/libraries/eventBridge';
import StripeApi from '~/libraries/stripeApi';
import { PaymentStatus } from '~/types/PaymentStatus';
import { StripeConfig } from '~/types/ProviderConfig';
import { Transaction } from '~/types/Transaction';

const callback = async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  if (req.method === 'POST') {
    try {
      const event = req.body;
      const transaction = await getTransactionFromPayload(event);
      if (transaction && transaction.status !== PaymentStatus.PENDING) {
        throw Error('Transaction alreay treated');
      } else if (!transaction) {
        throw Error('Transaction not found');
      }
      const configs = await getProviderConfigsByAppId(transaction.appId);
      const stripeConfig = configs?.find(
        (item) => item.type === 'Stripe',
      ) as StripeConfig;

      const stripe = StripeApi(stripeConfig);
      const app = await getApp(transaction.appId);

      switch (event.type) {
        case 'payment_intent.succeeded': {
          const paymentIntent = event.data.object;
          const paymentMethodType = paymentIntent.payment_method_types[0];
          const balanceTransactionId =
            paymentIntent.charges.data[0].balance_transaction;
          const balance = await stripe.balanceTransactions.retrieve(
            balanceTransactionId,
          );
          const transactionUpdated = await updateTransaction(transaction.id, {
            ...transaction,
            paymentServiceProvider: 'Stripe',
            fee: balance.fee,
            amount: transaction.amount,
            netAmount: transaction.amount - balance.fee,
            paymentMethodType: paymentMethodType,
            succeededAt: new Date(paymentIntent.created * 1000),
            status: PaymentStatus.SUCCEEDED,
          });
          if (transactionUpdated && app) {
            await eventBridge.putPaymentEvent(
              app.callbackUrl,
              transactionUpdated,
            );
          }
          break;
        }
        case 'payment_intent.payment_failed': {
          const paymentIntent = event.data.object;
          const paymentMethodType = paymentIntent.payment_method_types[0];
          const transactionUpdated = await updateTransaction(transaction.id, {
            ...transaction,
            paymentServiceProvider: 'Stripe',
            paymentMethodType: paymentMethodType,
            succeededAt: null,
            status: PaymentStatus.FAILED,
          });
          if (transactionUpdated && app) {
            await eventBridge.putPaymentEvent(
              app.callbackUrl,
              transactionUpdated,
            );
          }
          break;
        }
        case 'source.chargeable': {
          const source = event.data.object;
          stripe.charges.create({
            amount: source.amount,
            currency: 'eur',
            source: source.id,
          });
          break;
        }
        default:
      }
    } catch (err) {
      console.log(err.message);
      res.status(400).send(`Webhook Error: ${err.message}`);
      return;
    }
    res.status(200).send('OK');
  } else {
    res.setHeader('Allow', 'POST');
    res.status(405).end('Method Not Allowed');
  }
};

const getTransactionFromPayload = async (event: any): Promise<Transaction> => {
  switch (event.type) {
    case 'payment_intent.succeeded':
    case 'payment_intent.payment_failed': {
      const { id } = event.data.object;
      const transaction = await getTransactionByProvider(id);
      if (!transaction) {
        throw Error('Transaciton not found');
      }
      return transaction as Transaction;
    }
    default:
      throw Error('Not listening for this event type');
  }
};

export default callback;
