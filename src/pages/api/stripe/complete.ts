import { NextApiRequest, NextApiResponse } from 'next';
import getApp from 'src/graphql/app/helpers/getApp';
import getTransaction from 'src/graphql/transaction/helpers/getTransaction';
import getTransactionByProvider from 'src/graphql/transaction/helpers/getTransactionByProvider';

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const complete = async (req: NextApiRequest, res: NextApiResponse) => {
  const { payment_intent, redirect_status, token } = req.query;
  let status = 'failed';
  let transaction = null;
  if (payment_intent && redirect_status) {
    // Bancontact
    if (redirect_status === 'failed') {
      status = 'failed';
    } else if (redirect_status === 'succeeded') {
      status = 'succeeded';
    } else {
      status = 'failed';
    }
    transaction = await getTransactionByProvider(payment_intent as string);
  } else if (token) {
    // Card payment or Google payment
    status = 'succeeded';
    transaction = await getTransaction(token as string);
  }

  if (transaction) {
    const app = await getApp(transaction.appId);
    const params = new URLSearchParams({
      pagi_id: transaction.id,
      status,
    });
    res.redirect(301, `${app?.returnUrl}?${params}`);
  }
};

export default complete;
