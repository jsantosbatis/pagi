import { ApolloServer } from 'apollo-server-micro';
import jwt from 'jsonwebtoken';
import jwkToPem from 'jwk-to-pem';
import jwks from 'jwks.json';
import withCors from 'src/middlewares/withCors';
import schema from '../../graphql/schema';

const apolloServer = new ApolloServer({
  schema,
  introspection: true,
  playground: true,
  context: async ({ req }) => {
    return new Promise((resolve, _reject) => {
      // Get the user token from the headers.
      if (req.headers.authorization) {
        const [, token] = (req.headers.authorization as string).split(' ');
        const pem = jwkToPem(jwks.keys[1] as any);
        jwt.verify(token, pem, function (_err, decodedToken) {
          const user = { id: (decodedToken! as any).sub };
          resolve({ user, token });
        });
      }
      resolve({});
    });
  },
});

export const config = {
  api: {
    bodyParser: false,
  },
};

export default withCors(apolloServer.createHandler({ path: '/api/graphql' }));
