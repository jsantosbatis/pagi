import Amplify, { withSSRContext } from 'aws-amplify';
import type { NextApiRequest, NextApiResponse } from 'next';

// Amplify SSR configuration needs to be enabled within each API route
Amplify.configure({
  aws_user_files_s3_bucket: process.env.NEXT_PUBLIC_PAGI_AWS_BUCKET,
  aws_user_files_s3_bucket_region: process.env.NEXT_PUBLIC_PAGI_AWS_REGION,
  aws_cognito_identity_pool_id:
    process.env.NEXT_PUBLIC_PAGI_AWS_IDENTITY_POOL_ID,
  aws_cognito_region: process.env.NEXT_PUBLIC_PAGI_AWS_REGION,
  aws_user_pools_id: process.env.NEXT_PUBLIC_PAGI_AWS_USER_POOL_ID,
  aws_user_pools_web_client_id: process.env.NEXT_PUBLIC_PAGI_AWS_APP_CLIENT_ID,
  ssr: true,
});

export default async (req: NextApiRequest, res: NextApiResponse) => {
  const { Auth } = withSSRContext({ req });
  try {
    const user = await Auth.currentAuthenticatedUser();
    res.json({ user: user.username });
  } catch (err) {
    res.statusCode = 200;
    res.json({ user: null });
  }
};
