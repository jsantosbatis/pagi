import { NextApiRequest, NextApiResponse } from 'next';
import getApp from 'src/graphql/app/helpers/getApp';
import getTransactionByProvider from 'src/graphql/transaction/helpers/getTransactionByProvider';

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const complete = async (req: NextApiRequest, res: NextApiResponse) => {
  const { token, status } = req.query;

  const transaction = await getTransactionByProvider(token as string);
  if (transaction) {
    const app = await getApp(transaction.appId);
    const params = new URLSearchParams({
      pagi_id: transaction.id,
      status: status as string,
    });
    res.redirect(301, `${app?.returnUrl}?${params}`);
  }
};

export default complete;
