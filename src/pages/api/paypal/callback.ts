import paypal from '@paypal/checkout-server-sdk';
import { NextApiRequest, NextApiResponse } from 'next';
import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import getTransactionByProvider from 'src/graphql/transaction/helpers/getTransactionByProvider';
import updateTransaction from 'src/graphql/transaction/helpers/updateTransaction';
import eventBridge from '~/libraries/eventBridge';
import { Paypal } from '~/libraries/paypal';
import { PaymentStatus } from '~/types/PaymentStatus';
import { PaypalConfig } from '~/types/ProviderConfig';
import { decimalToInt } from '~/util';

const callback = async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  if (req.method === 'POST') {
    try {
      const event: any = req.body;
      if (event.event_type !== 'CHECKOUT.ORDER.APPROVED') {
        throw Error('Not listening for this event type');
      }
      let order = event.resource;
      const transaction = await getTransactionByProvider(order.id);
      if (transaction && transaction.status !== PaymentStatus.PENDING) {
        throw Error('Transaction alreay treated');
      } else if (!transaction) {
        throw Error('Transaction not found');
      }
      const app = await getApp(transaction.appId);
      const configs = await getProviderConfigsByAppId(transaction.appId);

      const paypalConfig = configs?.find(
        (item) => item.type === 'Paypal',
      ) as PaypalConfig;
      const paypalInstance = new Paypal({
        ...paypalConfig,
        isProduction: app?.isProduction,
      });

      const isValidWebhook = paypalInstance.verifyWebhook(
        req.headers,
        paypalConfig.webhookId,
        event,
      );

      if (!isValidWebhook) {
        throw Error('Unvalid webhook');
      }

      if (order.status !== 'COMPLETED') {
        const request = new paypal.orders.OrdersCaptureRequest(order.id);
        request.requestBody({});
        // Call API with your client and get a response for your call
        order = (await paypalInstance.client.execute(request)).result;
      }

      const capture = order.purchase_units[0].payments.captures[0];
      let paypalFee = capture.seller_receivable_breakdown.paypal_fee.value;
      paypalFee = decimalToInt(paypalFee);

      const transactionUpdated = await updateTransaction(transaction.id, {
        ...transaction,
        paymentServiceProvider: 'Paypal',
        fee: paypalFee,
        amount: transaction.amount,
        netAmount: transaction.amount - paypalFee,
        paymentMethodType: 'paypal',
        succeededAt: new Date(event.create_time),
        status: PaymentStatus.SUCCEEDED,
      });
      if (transactionUpdated && app) {
        await eventBridge.putPaymentEvent(app.callbackUrl, transactionUpdated);
      }
    } catch (err) {
      res.status(400).send(`Webhook Error: ${err.message}`);
      return;
    }
    res.status(200).send('OK');
  } else {
    res.setHeader('Allow', 'POST');
    res.status(405).end('Method Not Allowed');
  }
};

export default callback;
