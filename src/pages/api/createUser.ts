import { NextApiRequest, NextApiResponse } from 'next';
import createUser from 'src/graphql/user/helpers/createUser';

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
/*
{
  version: '1',
  region: 'eu-west-2',
  userPoolId: 'eu-west-2_XDXx2P9yL',
  userName: 'ddab6b1c-6cb8-48fc-8442-e6c0e0a31765',
  callerContext: {
    awsSdkVersion: 'aws-sdk-unknown-unknown',
    clientId: '2tu57akk6rchgrah3qkh2b6mnf'
  },
  triggerSource: 'PostConfirmation_ConfirmSignUp',
  request: {
    userAttributes: {
      sub: 'ddab6b1c-6cb8-48fc-8442-e6c0e0a31765',
      'cognito:email_alias': 'paulobraga7@msn.com',
      'cognito:user_status': 'CONFIRMED',
      email_verified: 'true',
      phone_number_verified: 'false',
      phone_number: '+32477591305',
      email: 'paulobraga7@msn.com'
    }
  },
  response: {}
}
*/

type Event = {
  version: string;
  region: string;
  userPoolId: string;
  userName: string;
  callerContext: {
    awsSdkVersion: string;
    clientId: string;
  };
  triggerSource: string;
  request: {
    userAttributes: {
      sub: string;
      'cognito:email_alias': string;
      'cognito:user_status': 'CONFIRMED' | string;
      email_verified: boolean;
      phone_number_verified: boolean;
      phone_number: string;
      email: string;
    };
  };
};
export default async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  const event: Event = req.body;
  const { userAttributes } = event.request;
  const user = await createUser(userAttributes.sub, {
    email: userAttributes.email,
  } as any);
  if (user) {
    res.statusCode = 200;
    res.send('OK');
  } else {
    res.statusCode = 400;
    res.send('KO');
  }
};
