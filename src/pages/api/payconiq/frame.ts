/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable-next-line @typescript-eslint/no-unused-vars */
import axios from 'axios';
import { NextApiRequest, NextApiResponse } from 'next';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'POST') {
    res.status(405).send(`Use POST instead of ${req.method}`);
  } else {
    const { url } = req.body;
    if (!url) {
      res.status(400).send('url is missing');
      return;
    }

    /*const frame = (
      await axios({
        url: `${process.env.NEXT_PUBLIC_VERCEL_URL}/payconiq-frame.png`,
        responseType: 'arraybuffer',
      })
    ).data as Buffer;*/

    const qrCode = (
      await axios({
        url: url.replace(/s=(L|S)/gm, 's=XL'),
        responseType: 'arraybuffer',
      })
    ).data as Buffer;
    /*
    const buffer = await sharp(frame)
      .toFormat('png')
      .composite([
        {
          input: qrCode,
          top: 300,
          left: 100,
        },
      ])
      .toBuffer();*/
    res.setHeader('Content-Type', 'image/png');
    res.send(qrCode);
  }
};
