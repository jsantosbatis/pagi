import { NextApiRequest, NextApiResponse } from 'next';
import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import getTransaction from 'src/graphql/transaction/helpers/getTransaction';
import { Payconiq } from '~/libraries/payconiq';
import { PayconiqConfig } from '~/types/ProviderConfig';

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const complete = async (req: NextApiRequest, res: NextApiResponse) => {
  const { token } = req.query;
  let status: string;
  const transaction = await getTransaction(token as string);
  if (transaction) {
    const app = await getApp(transaction.appId);
    const configs = await getProviderConfigsByAppId(transaction.appId);
    const payconiqConfig = configs?.find(
      (item) => item.type === 'Payconiq',
    ) as PayconiqConfig;

    const payconiq = new Payconiq({
      ...payconiqConfig,
      isProduction: app ? app.isProduction : false,
    });
    const payment = await payconiq.getPaymentDetails(
      transaction.providerReference,
    );

    if (payment.status === 'SUCCEEDED') {
      status = 'succeeded';
    } else {
      status = 'failed';
    }
    const params = new URLSearchParams({
      pagi_id: transaction.id,
      status,
    });
    res.redirect(301, `${app?.returnUrl}?${params}`);
    return;
  }
  res.status(400);
};

export default complete;
