import { NextApiRequest, NextApiResponse } from 'next';
import getApp from 'src/graphql/app/helpers/getApp';
import getPayconiqQRCode from 'src/graphql/payconiq/helpers/getPayconiqQRCode';
import createTransaction from 'src/graphql/transaction/helpers/createTransaction';
import getTransaction from 'src/graphql/transaction/helpers/getTransaction';
import getTransactionByProvider from 'src/graphql/transaction/helpers/getTransactionByProvider';
import updateTransaction from 'src/graphql/transaction/helpers/updateTransaction';
import eventBridge from '~/libraries/eventBridge';
import { PaymentDetail } from '~/libraries/payconiq';
import { PaymentStatus } from '~/types/PaymentStatus';

const PAYCONIQ_ONLINE_FEE = 20;
const PAYCONIQ_OFFLINE_FEE = 6;
const callback = async (
  req: NextApiRequest,
  res: NextApiResponse,
): Promise<void> => {
  if (req.method === 'POST') {
    try {
      const event: PaymentDetail = req.body;
      const staticQRCode = await getPayconiqQRCode(event.reference);
      if (staticQRCode) {
        let transactionCreated = await createTransaction(staticQRCode.appId, {
          amount: event.totalAmount,
          appId: staticQRCode.appId,
          description: staticQRCode.description,
          customerReference: staticQRCode.customerReference,
          providerReference: event.paymentId,
          netAmount: event.totalAmount - PAYCONIQ_OFFLINE_FEE,
          fee: PAYCONIQ_OFFLINE_FEE,
          paymentMethodType: 'payconiq',
          succeededAt: new Date(event.succeededAt),
          status: PaymentStatus.SUCCEEDED,
          paymentServiceProvider: 'Payconiq',
          products: [staticQRCode.product],
        } as any);
        if (!transactionCreated) {
          throw Error('Transaction cannot be created for this static qr code');
        }
        const app = await getApp(transactionCreated.appId);
        transactionCreated = await getTransaction(transactionCreated.id);
        if (app && transactionCreated) {
          await eventBridge.putPaymentEvent(
            app.callbackUrl,
            transactionCreated,
          );
        }
      } else {
        const transaction = await getTransactionByProvider(event.paymentId);
        if (transaction && transaction.status !== PaymentStatus.PENDING) {
          throw Error('Transaction alreay treated');
        } else if (!transaction) {
          throw Error('Transaction not found');
        }
        const transactionUpdated = await updateTransaction(transaction.id, {
          ...transaction,
          paymentServiceProvider: 'Payconiq',
          fee: PAYCONIQ_ONLINE_FEE,
          amount: event.totalAmount,
          netAmount: event.totalAmount - PAYCONIQ_ONLINE_FEE,
          paymentMethodType: 'payconiq',
          succeededAt: new Date(event.succeededAt),
          status: PaymentStatus.SUCCEEDED,
        });
        const app = await getApp(transaction.appId);
        if (transactionUpdated && app) {
          await eventBridge.putPaymentEvent(
            app.callbackUrl,
            transactionUpdated,
          );
        }
      }
    } catch (err) {
      res.status(400).send(`Webhook Error: ${err.message}`);
      return;
    }
    res.status(200).send('OK');
  } else {
    res.setHeader('Allow', 'POST');
    res.status(405).end('Method Not Allowed');
  }
};
export default callback;
