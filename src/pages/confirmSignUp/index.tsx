import { FunctionComponent, useEffect, useState } from 'react';
import { Auth } from 'aws-amplify';
import { useForm } from 'react-hook-form';
import ErrorAlert from '~/components/ErrorAlert';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';

const ConfirmSignUp: FunctionComponent<any> = () => {
  const {
    register,
    handleSubmit,
    formState: { isValid },
  } = useForm({ mode: 'onChange' });
  const [error, setError] = useState<string>('');
  const [emailInput, setEmailInput] = useState('');
  const router = useRouter();
  const { email } = router.query;
  const { t } = useTranslation();

  useEffect(() => {
    setEmailInput(email as string);
  }, []);

  const onSubmit = async (data: any) => {
    try {
      try {
        await Auth.confirmSignUp(emailInput, data.code);
        router.push('explore');
      } catch (error) {
        setError(error.message);
      }
    } catch (e) {
      console.error(e);
    }
  };
  return (
    <div className="min-h-screen bg-gray-50 flex flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-md">
        <h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
          {t('confirmSignUp.title')}
        </h2>
      </div>
      <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          <form className="space-y-6" onSubmit={handleSubmit(onSubmit)}>
            <div>
              <label
                htmlFor="email"
                className="block text-sm font-medium text-gray-700"
              >
                Email
              </label>
              <div className="mt-1">
                <input
                  id="email"
                  name="email"
                  type="email"
                  autoComplete="email"
                  readOnly
                  className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm"
                  value={emailInput}
                />
              </div>
            </div>
            <div>
              <label
                htmlFor="code"
                className="block text-sm font-medium text-gray-700"
              >
                Code
              </label>
              <div className="mt-1">
                <input
                  id="code"
                  name="code"
                  type="code"
                  ref={register({ required: true })}
                  className="appearance-none block w-full px-3 py-2 border border-gray-300 rounded-md shadow-sm placeholder-gray-400 focus:outline-none focus:ring-primary-500 focus:border-primary-500 sm:text-sm"
                />
              </div>
            </div>

            <div>
              <button
                type="submit"
                disabled={!isValid}
                className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-primary-600 hover:bg-primary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:opacity-40"
              >
                {t('confirmSignUp.confirm')}
              </button>
            </div>
            {error && <ErrorAlert errors={[error]} />}
          </form>
        </div>
      </div>
    </div>
  );
};

export default ConfirmSignUp;
