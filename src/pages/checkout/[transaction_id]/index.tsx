import { FunctionComponent } from 'react';
import { Elements } from '@stripe/react-stripe-js';

import stripePromise from '~/libraries/stripe';
import Checkout from '~/components/Checkout';
import { useRouter } from 'next/router';
import { useQuery } from '@apollo/client';
import CheckoutContext from '~/contexts/checkout-context';
import { StripeConfig } from '~/types/ProviderConfig';
import CheckoutWrapper from '~/components/CheckoutWrapper';
import { Transaction } from '~/types/Transaction';
import { PaymentStatus } from '~/types/PaymentStatus';
import CheckoutExpired from '~/components/CheckoutExpired';
import CheckoutError from '~/components/CheckoutError';
import CheckoutSkeleton from '~/components/CheckoutSkeleton';
import getTransaction from 'src/graphql/queries/getTransaction';

const Payment: FunctionComponent = () => {
  const router = useRouter();
  const { transaction_id, lang } = router.query;
  const { loading, data, error } = useQuery(getTransaction(), {
    variables: { id: transaction_id },
    skip: !transaction_id,
  });

  const getStripeConfig = (): StripeConfig => {
    return data.transactionById.providerConfigs.find(
      (item: any) => item.__typename === 'ProviderConfigStripe',
    );
  };
  if (error) return <CheckoutWrapper checkoutComponent={<CheckoutError />} />;
  if (loading || !data)
    return <CheckoutWrapper checkoutComponent={<CheckoutSkeleton />} />;
  if (
    data &&
    (data.transactionById as Transaction).status == PaymentStatus.SUCCEEDED
  ) {
    return <CheckoutWrapper checkoutComponent={<CheckoutExpired />} />;
  }
  if (data && data.transactionById) {
    return (
      <Elements stripe={stripePromise(getStripeConfig(), lang as string)}>
        <CheckoutContext.Provider
          value={{
            stripePublicKey: getStripeConfig().publicKey,
            providerConfigs: data.transactionById.providerConfigs,
          }}
        >
          <CheckoutWrapper
            name={data.transactionById.app.name}
            image={data.transactionById.app.image}
            checkoutComponent={
              <Checkout
                transaction={data.transactionById}
                lang={lang as string}
              />
            }
          />
        </CheckoutContext.Provider>
      </Elements>
    );
  } else {
    return <CheckoutWrapper checkoutComponent={<CheckoutError />} />;
  }
};

export default Payment;
