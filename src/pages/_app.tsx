import Amplify from 'aws-amplify';
import '../styles/global.css';
import { ApolloProvider } from '@apollo/client';
import client from '~/libraries/apolloClient';
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import translationEN from '../translations/en/translation.json';
import translationFR from '../translations/fr/translation.json';
import { ReactNode } from 'react';

import LanguageDetector from 'i18next-browser-languagedetector';

Amplify.configure({
  aws_user_files_s3_bucket: process.env.NEXT_PUBLIC_PAGI_AWS_BUCKET,
  aws_user_files_s3_bucket_region: process.env.NEXT_PUBLIC_PAGI_AWS_REGION,
  aws_cognito_identity_pool_id:
    process.env.NEXT_PUBLIC_PAGI_AWS_IDENTITY_POOL_ID,
  aws_cognito_region: process.env.NEXT_PUBLIC_PAGI_AWS_REGION,
  aws_user_pools_id: process.env.NEXT_PUBLIC_PAGI_AWS_USER_POOL_ID,
  aws_user_pools_web_client_id: process.env.NEXT_PUBLIC_PAGI_AWS_APP_CLIENT_ID,
  ssr: true,
});

/*import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';*/
// don't want to use this?
// have a look at the Quick start guide
// for passing in lng and translations on init

const resources = {
  en: {
    translation: translationEN,
  },
  fr: {
    translation: translationFR,
  },
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    detection: {
      lookupQuerystring: 'lang',
    },
    keySeparator: '.',
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false, // react already safes from xss
    },
  });

export default function MyApp({ Component, pageProps }: any): ReactNode {
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  );
}
