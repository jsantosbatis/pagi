import TileApp from '~/components/TileApp';
import { useQuery } from '@apollo/client';
import { App } from '~/types/App';
import CreateApp from 'src/components/CreateApp';
import CreateAppContext from '../../contexts/createApp-context';
import { FunctionComponent, useState } from 'react';
import getAppsQuery from 'src/graphql/queries/getApps';
import Layout from 'src/components/Layout';
import WithAuth from '~/components/WithAuth';

const Explore: FunctionComponent = () => {
  const { loading, data } = useQuery(getAppsQuery());
  const [isCreateAppOpen, setIsCreateAppOpen] = useState(false);
  const toggleCreateApp = (value: boolean) => {
    setIsCreateAppOpen(value);
  };

  return (
    <CreateAppContext.Provider
      value={{ isCreateAppOpen, toggleCreateApp: toggleCreateApp as any }}
    >
      <Layout
        header={
          <div className="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
            <div className="flex justify-between">
              <h1 className="text-3xl font-bold text-white">Your apps</h1>
              <button
                type="button"
                className="inline-flex items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500"
                onClick={() => toggleCreateApp(true)}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                  className="-ml-1 mr-2 h-5 w-5"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth={2}
                    d="M12 6v6m0 0v6m0-6h6m-6 0H6"
                  />
                </svg>
                New app
              </button>
            </div>
          </div>
        }
        body={
          <>
            <div className="max-w-5xl mx-auto pb-6 px-4 sm:px-6 lg:pb-16 lg:px-8">
              <div className="grid grid-cols-1 sm:grid-cols-3 gap-4">
                {loading ? (
                  ''
                ) : (
                  <>
                    {data.apps.map((item: App) => (
                      <TileApp key={item.id} app={item} />
                    ))}
                  </>
                )}
              </div>
            </div>
            <CreateApp />
          </>
        }
      />
    </CreateAppContext.Provider>
  );
};

export default WithAuth(Explore);
