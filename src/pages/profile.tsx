import { useState, useEffect } from 'react';
import { Auth } from 'aws-amplify';
import { withAuthenticator, AmplifySignOut } from '@aws-amplify/ui-react';

type User = {
  username: string;
};

function Profile() {
  const [user, setUser] = useState<User | null>(null);
  useEffect(() => {
    // Access the user session on the client
    Auth.currentAuthenticatedUser()
      .then((data) => {
        console.log(`User: `, data);
        setUser(data);
      })
      .catch(() => setUser(null));
  }, []);
  return (
    <div>
      {user && <h1>Welcome, {user.username}</h1>}
      <AmplifySignOut />
    </div>
  );
}

export default withAuthenticator(Profile);
