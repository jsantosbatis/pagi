import Layout from '~/components/Layout';
import { useRouter } from 'next/router';
import { useQuery } from '@apollo/client';
import getTransactionHistory from 'src/graphql/queries/getTransactionHistory';
import { FunctionComponent } from 'react';
import { PaymentStatus } from '~/types/PaymentStatus';
import dayjs from 'dayjs';
import { intToDecimal } from '~/util';
import NumberFormat from 'react-number-format';
import { TransactionHistory } from '~/types/TransactionHistory';
import { CashIcon } from '@heroicons/react/solid';
import TableSkeleton from '~/components/TableSkeleton';
import WithAuth from '~/components/WithAuth';

const Home: FunctionComponent = () => {
  const router = useRouter();
  const { app_id } = router.query;
  const startDate = dayjs().subtract(1, 'd').unix() * 1000;
  const endDate = dayjs().unix() * 1000;

  const { loading, data } = useQuery(getTransactionHistory(), {
    variables: {
      appId: app_id,
      startDate,
      endDate,
    },
    skip: !app_id,
  });

  const statusStyles = {
    0: 'bg-yellow-100 text-yellow-800',
    1: 'bg-gray-100 text-gray-800',
    2: 'bg-green-100 text-green-800',
  };

  const classNames = (...classes: any[]) => {
    return classes.filter(Boolean).join(' ');
  };

  const renderStatus = (status: number) => {
    switch (status) {
      case PaymentStatus.FAILED:
        return 'Failed';
      case PaymentStatus.SUCCEEDED:
        return 'Succeeded';
      default:
        return 'Pending';
    }
  };

  const renderDate = (date: Date) => {
    return dayjs(date).format('D MMM HH:mm:ss');
  };

  return (
    <Layout
      header={
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl font-bold text-white">Dashboard</h1>
        </div>
      }
      body={
        <div className="max-w-screen-xl mx-auto pb-6 px-4 sm:px-6 lg:pb-16 lg:px-8">
          <div className="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
            {loading ? (
              <TableSkeleton />
            ) : (
              <div className="flex flex-col">
                <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                  <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                      <table className="min-w-full divide-y divide-gray-200">
                        <thead className="bg-gray-50">
                          <tr>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              id
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              amount
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              fee
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              net amount
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              status
                            </th>
                            <th
                              scope="col"
                              className="px-6 py-3 text-right text-xs font-medium text-gray-500 uppercase tracking-wider"
                            >
                              date
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {data &&
                            data.transactionHistory &&
                            data.transactionHistory.map(
                              (
                                transaction: TransactionHistory,
                                index: number,
                              ) => (
                                <tr
                                  key={transaction.id}
                                  className={
                                    index % 2 === 0 ? 'bg-white' : 'bg-gray-50'
                                  }
                                >
                                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-400 flex space-x-2">
                                    <CashIcon
                                      className="flex-shrink-0 h-5 w-5 text-gray-400"
                                      aria-hidden="true"
                                    />
                                    <span>{transaction.id}</span>
                                  </td>
                                  <td className="px-6 py-4 whitespace-nowrap text-right text-sm text-gray-500">
                                    <NumberFormat
                                      value={intToDecimal(transaction.amount)}
                                      displayType={'text'}
                                      thousandSeparator={true}
                                      suffix={'€'}
                                      className="tracking-wider"
                                    />
                                  </td>
                                  <td className="px-6 py-4 whitespace-nowrap text-right text-sm text-gray-500">
                                    <NumberFormat
                                      value={intToDecimal(transaction.fee)}
                                      displayType={'text'}
                                      thousandSeparator={true}
                                      suffix={'€'}
                                      className="tracking-wider"
                                    />
                                  </td>
                                  <td className="px-6 py-4 whitespace-nowrap font-bold text-right text-sm text-gray-500">
                                    <NumberFormat
                                      value={intToDecimal(
                                        transaction.netAmount,
                                      )}
                                      displayType={'text'}
                                      thousandSeparator={true}
                                      suffix={'€'}
                                      className="tracking-wider"
                                    />
                                  </td>
                                  <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                    <span
                                      className={classNames(
                                        statusStyles[transaction.status],
                                        'inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium capitalize',
                                      )}
                                    >
                                      {renderStatus(transaction.status)}
                                    </span>
                                  </td>
                                  <td className="px-6 py-4 whitespace-nowrap text-right text-sm text-gray-500">
                                    {renderDate(transaction.date)}
                                  </td>
                                </tr>
                              ),
                            )}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      }
    />
  );
};

export default WithAuth(Home);
