import { FunctionComponent, useEffect, useState } from 'react';
import AppForm from 'src/components/AppForm';
import Layout from '~/components/Layout';
import { useRouter } from 'next/router';
import { useQuery } from '@apollo/client';
import getAppQuery from 'src/graphql/queries/getApp';
import { App } from '~/types/App';
import { ProviderConfig } from '~/types/ProviderConfig';
import ConfigPayconiqForm from 'src/components/ConfigPayconiqForm';
import ConfigStripeForm from 'src/components/ConfigStripeForm';
import ConfigPaypalForm from 'src/components/ConfigPaypalForm';
import WithAuth from '~/components/WithAuth';

type AppWithProviders = App & {
  providerConfigs: ProviderConfig[];
};
const Settings: FunctionComponent = () => {
  const router = useRouter();
  const { app_id } = router.query;
  const { loading, data } = useQuery(getAppQuery(app_id as string));
  const [appData, setAppData] = useState<AppWithProviders | null>(null);
  useEffect(() => {
    if (data) {
      const { appById } = data;
      setAppData(appById);
    }
  }, [data]);
  return (
    <Layout
      header={
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <h1 className="text-3xl font-bold text-white">Settings</h1>
        </div>
      }
      body={
        <div className="max-w-screen-xl mx-auto pb-6 px-4 sm:px-6 lg:pb-16 lg:px-8">
          <div className="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <>
                <AppForm data={appData as App} />
                {appData?.providerConfigs.map((item) => {
                  switch (item.type) {
                    case 'Payconiq':
                      return <ConfigPayconiqForm key={item.id} data={item} />;
                    case 'Stripe':
                      return <ConfigStripeForm key={item.id} data={item} />;
                    case 'Paypal':
                      return <ConfigPaypalForm key={item.id} data={item} />;
                    default:
                      '';
                  }
                })}
              </>
            )}
          </div>
        </div>
      }
    />
  );
};

export default WithAuth(Settings);
