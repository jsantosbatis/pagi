import { v4 } from 'uuid';

export function generateId(): string {
  return v4().replace(/-/g, '');
}

/**
 * Convert int to decimal.
 * eg: 100 -> 1.00
 * eg: 1000 -> 10.00
 * @param {*} num
 */
export function intToDecimal(num: number): number {
  return +(num / 100).toFixed(2);
}

export function decimalToInt(num: number): number {
  return parseInt(num.toString().replace('.', ''));
}

export function getStripeCompleteUrl(token?: string): string {
  let url = `/api/stripe/complete`;
  if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('https')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('http')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else {
    url = `https://${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  }
  if (token) {
    return url.concat('?', `token=${token}`);
  }

  return url;
}

export function getPayconiqCompleteUrl(token?: string): string {
  let url = `/api/payconiq/complete`;
  if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('https')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('http')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else {
    url = `https://${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  }
  if (token) {
    return url.concat('?', `token=${token}`);
  }

  return url;
}

export function getPaypalCompleteUrl(status: string): string {
  let url = `/api/paypal/complete?status=${status}`;
  if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('https')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('http')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else {
    url = `https://${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  }

  return url;
}

export function getPayconiqCallbackUrl(): string {
  let url = `/api/payconiq/callback`;
  if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('https')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('http')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else {
    url = `https://${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  }
  return url;
}

export function getCheckoutUrl(transactionId: string, lang = 'en'): string {
  let url = `/checkout/${transactionId}?lang=${lang}`;
  if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('https')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else if (process.env.NEXT_PUBLIC_VERCEL_URL?.includes('http')) {
    url = `${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  } else {
    url = `https://${process.env.NEXT_PUBLIC_VERCEL_URL}${url}`;
  }
  return url;
}

export function toDate(moment: string | Date | null): Date | null {
  if (typeof moment === 'string') {
    return new Date(moment);
  }
  return moment;
}

export function getBucketUrl(imageKey: string): string {
  return `https://${process.env.NEXT_PUBLIC_PAGI_AWS_BUCKET}.s3.${process.env.NEXT_PUBLIC_PAGI_AWS_REGION}.amazonaws.com/public/${imageKey}`;
}
