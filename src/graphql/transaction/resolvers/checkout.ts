/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Transaction } from '~/types/Transaction';
import createTransaction from '../helpers/createTransaction';

export default async function checkout(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<Transaction | null> {
  const { appId, transactionInput, lang } = args;
  return createTransaction(appId, transactionInput, lang);
}
