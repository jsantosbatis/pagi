/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { TransactionHistory } from '~/types/TransactionHistory';
import getTransactionHistory from '../helpers/getTransactionHistory';

export default async function transactionHistory(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<TransactionHistory[] | null> {
  const { appId, startDate, endDate } = args;
  return getTransactionHistory(appId, startDate, endDate);
}
