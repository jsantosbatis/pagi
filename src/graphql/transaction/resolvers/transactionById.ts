/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Transaction } from '~/types/Transaction';
import getTransaction from '../helpers/getTransaction';

export default async function transactionById(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<Transaction | null> {
  const { id } = args;
  return getTransaction(id);
}
