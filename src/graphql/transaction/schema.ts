import { makeExecutableSchema } from '@graphql-tools/schema';
import { ProviderConfig } from '~/types/ProviderConfig';
import { Transaction } from '~/types/Transaction';
import getApp from '../app/helpers/getApp';
import getProviderConfigsByAppId from '../app/helpers/getProviderConfigsByAppId';
import commonTypeDefs from '../common/typedefs';
import checkout from './resolvers/checkout';
import transactionById from './resolvers/transactionById';
import transactionHistory from './resolvers/transactionHistory';
import typedefs from './typedefs';

const userSchema = makeExecutableSchema({
  typeDefs: [typedefs, commonTypeDefs],
  resolvers: {
    Query: {
      transactionById,
      transactionHistory,
    },
    Mutation: {
      checkout,
    },
    Transaction: {
      providerConfigs: (transaction: Transaction) => {
        if (transaction.appId) {
          return getProviderConfigsByAppId(transaction.appId);
        } else {
          return null;
        }
      },
      app: (transaction: Transaction) => {
        if (transaction.appId) {
          return getApp(transaction.appId);
        } else {
          return null;
        }
      },
    },
    ProviderConfig: {
      __resolveType(source: ProviderConfig) {
        switch (source.type) {
          case 'Payconiq':
            return 'ProviderConfigPayconiq';
          case 'Stripe':
            return 'ProviderConfigStripe';
          case 'Paypal':
            return 'ProviderConfigPaypal';
        }
      },
    },
  },
});

export default userSchema;
