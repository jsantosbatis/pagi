/* eslint-disable no-console */
import TransactionHistorySchema from '~/db/schemas/TransactionHistory';
import { PaymentStatus } from '~/types/PaymentStatus';
import { Transaction } from '~/types/Transaction';
import { TransactionHistory } from '~/types/TransactionHistory';

export default async function updateTransactionHistory(
  transaction: Transaction,
): Promise<TransactionHistory | null> {
  const response = await TransactionHistorySchema.update(
    {
      PK: `transaction_history_${transaction.id}`,
      SK: `app_${transaction.appId}`,
    },
    {
      GSI3PK: `app_${transaction.appId}`,
      GSI3SKTransactionDate:
        transaction.status === PaymentStatus.SUCCEEDED
          ? new Date(transaction.succeededAt as Date).getTime()
          : new Date(transaction.createdAt as Date).getTime(),
      amount: transaction.amount,
      fee: transaction.fee,
      netAmount: transaction.netAmount,
      paymentMethodType: transaction.paymentMethodType,
      paymentServiceProvider: transaction.paymentServiceProvider,
      status: transaction.status,
      description: transaction.description,
    },
  );

  if (response) {
    return {
      ...response.toJSON(),
    } as TransactionHistory;
  }
  return null;
}
