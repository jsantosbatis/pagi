/* eslint-disable no-console */
import { omit } from 'lodash';
import TransactionSchema from '~/db/schemas/Transaction';
import { Transaction } from '~/types/Transaction';
import { toDate } from '~/util';
import getTransaction from './getTransaction';
import updateTransactionHistory from './updateTransactionHistory';

export default async function updateTransaction(
  id: string,
  transaction: Transaction,
): Promise<Omit<Transaction, 'createdAt' | 'updatedAt'> | null> {
  const originTransaction = await getTransaction(id);
  if (!originTransaction) {
    throw new Error('Transaction not found');
  }

  const response = await TransactionSchema.update(
    { PK: originTransaction.PK, SK: originTransaction.SK },
    {
      ...omit(transaction, [
        'createdAt',
        'updatedAt',
        'SK',
        'PK',
        'GSI1SK',
        'GSI1PK',
      ]),
      succeededAt: toDate(transaction.succeededAt),
      products: JSON.stringify(transaction.products),
    },
  );

  if (response) {
    const parsed = response.toJSON();
    console.log(parsed);
    await updateTransactionHistory(parsed);
    return {
      ...parsed,
      products: JSON.parse(parsed.products),
    } as Transaction;
  }
  return null;
}
