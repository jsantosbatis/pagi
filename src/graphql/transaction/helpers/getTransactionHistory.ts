import dayjs from 'dayjs';
import TransactionHistorySchema from '~/db/schemas/TransactionHistory';
import { TransactionHistory } from '~/types/TransactionHistory';

export default async function getTransactionHistory(
  appId: string,
  startDate: number,
  endDate: number,
): Promise<TransactionHistory[] | null> {
  const response = await TransactionHistorySchema.query({
    GSI3PK: { eq: `app_${appId}` },
    GSI3SKTransactionDate: { between: [startDate, endDate] },
  })
    .sort('descending')
    .using('GSI3TransactionHistory')
    .exec();

  if (response['count'] > 0) {
    return response.toJSON().map((item: TransactionHistory) => ({
      ...item,
      date: dayjs(item.GSI3SKTransactionDate).toISOString(),
      id: item.PK?.split('_')[2],
    })) as TransactionHistory[];
  }
  return null;
}
