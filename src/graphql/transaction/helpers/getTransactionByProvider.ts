import TransactionSchema from '~/db/schemas/Transaction';
import { Transaction } from '~/types/Transaction';

export default async function getTransactionByProvider(
  providerReference: string,
): Promise<Transaction | null> {
  const response = await TransactionSchema.query({
    GSI2PK: { eq: providerReference },
  })
    .using('GSI2TransactionByProviderReference')
    .exec();

  const [transaction] = response;
  if (transaction) {
    const parsed = transaction.toJSON();
    return {
      ...parsed,
      products: JSON.parse(parsed.products),
    } as Transaction;
  }
  return null;
}
