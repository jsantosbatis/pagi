import getApp from 'src/graphql/app/helpers/getApp';
import { generateId, getCheckoutUrl, toDate } from 'src/util';
import TransactionSchema from '~/db/schemas/Transaction';
import { PaymentStatus } from '~/types/PaymentStatus';
import { Transaction } from '~/types/Transaction';
import createTransactionHistory from './createTransactionHistory';

export default async function createTransaction(
  appId: string,
  transaction: Transaction,
  lang = 'en',
): Promise<Transaction | null> {
  const originApp = await getApp(appId);
  if (!originApp) {
    throw new Error('App not found');
  }

  const id = generateId();
  const response = await TransactionSchema.create({
    PK: `transaction_${id}`,
    SK: `transaction`,
    GSI1PK: `app_${appId}`,
    GSI1SK: `transaction_${id}`,
    ...transaction,
    succeededAt: toDate(transaction.succeededAt),
    products: JSON.stringify(transaction.products),
    status: transaction.status ?? PaymentStatus.PENDING,
    appId,
    id,
  });

  if (response) {
    const parsed = response.toJSON();
    await createTransactionHistory(parsed);
    return {
      ...parsed,
      products: JSON.parse(parsed.products),
      url: getCheckoutUrl(id, lang),
    } as Transaction;
  }
  return null;
}
