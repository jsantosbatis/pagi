import TransactionSchema from '~/db/schemas/Transaction';
import { Transaction } from '~/types/Transaction';

export default async function getTransaction(
  transactionId: string,
): Promise<Transaction | null> {
  const response = await TransactionSchema.query({
    PK: { eq: `transaction_${transactionId}` },
    SK: { eq: 'transaction' },
  }).exec();

  const [transaction] = response;
  if (transaction) {
    const parsed = transaction.toJSON();
    return {
      ...parsed,
      products: JSON.parse(parsed.products),
    } as Transaction;
  }
  return null;
}
