import { gql } from 'apollo-server-micro';

export default gql`
  type AppTransaction {
    id: ID
    name: String
    image: String
  }

  type Transaction {
    id: ID
    appId: ID
    paymentServiceProvider: String
    customerReference: String
    providerReference: String
    description: String
    paymentMethodType: String
    status: Int
    amount: Int
    netAmount: Int
    fee: Int
    succeededAt: String
    createdAt: String
    updatedAt: String
    url: String
    providerConfigs: [ProviderConfig]
    products: [Product]
    app: AppTransaction
  }

  type TransactionHistory {
    id: String
    paymentServiceProvider: String
    paymentMethodType: String
    amount: Int
    netAmount: Int
    fee: Int
    status: Int
    date: String
    description: String
  }

  input TransactionInput {
    amount: Int
    description: String
    customerReference: String
    products: [ProductInput]!
  }

  type Mutation {
    checkout(
      appId: ID!
      transactionInput: TransactionInput!
      lang: String = "en"
    ): Transaction
  }

  type Query {
    transactionById(id: ID!): Transaction
    transactionHistory(
      appId: ID!
      startDate: Float!
      endDate: Float!
    ): [TransactionHistory]
  }
`;
