import db from '~/db/client';
import { User } from '~/types/User';
import getUser from './getUser';

export default async function updateUser(
  id: string,
  user: User,
): Promise<User | null> {
  const originUser = await getUser(id);
  if (!originUser) {
    throw new Error('User not found');
  }
  const response = await db.update(
    { PK: originUser.PK, SK: 'user' },
    { ...user },
  );
  if (response) {
    return {
      ...response.toJSON(),
    } as User;
  }
  return null;
}
