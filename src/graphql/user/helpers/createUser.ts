import getUser from 'src/graphql/user/helpers/getUser';
import UserSchema from '~/db/schemas/User';
import { User } from '~/types/User';

export default async function createUser(
  id: string,
  user: User,
): Promise<User | null> {
  const originUser = await getUser(id);
  if (originUser) {
    throw new Error('User already exist');
  }
  const response = await UserSchema.create({
    PK: `user_${id}`,
    SK: `user`,
    ...user,
    id,
  });

  if (response) {
    return {
      ...response.toJSON(),
    } as User;
  }
  return null;
}
