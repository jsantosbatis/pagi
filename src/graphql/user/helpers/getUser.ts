import db from '~/db/client';
import { User } from '~/types/User';

export default async function getUser(id: string): Promise<User | null> {
  const response = await db.get({ PK: `user_${id}`, SK: 'user' });
  if (response) {
    return {
      ...response.toJSON(),
      id,
    } as User;
  }
  return null;
}
