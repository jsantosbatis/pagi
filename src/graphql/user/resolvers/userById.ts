/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { User } from '~/types/User';
import getUser from '../helpers/getUser';

export default async function userById(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<User | null> {
  const { id } = args;
  return getUser(id);
}
