/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { User } from '~/types/User';
import { default as updateUserHelper } from '../helpers/updateUser';

export default async function updateUser(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<User | null> {
  const { id, userInput } = args;
  return updateUserHelper(id, userInput);
}
