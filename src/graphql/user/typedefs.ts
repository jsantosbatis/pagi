import { gql } from 'apollo-server-micro';

export default gql`
  type User {
    id: ID
    firstName: String
    lastName: String
    vatNumber: String
    street: String
    box: String
    city: String
    postalCode: String
    email: String
    createdAt: String
    updatedAt: String
  }

  input UserInput {
    firstName: String
    lastName: String
    vatNumber: String
    street: String
    box: String
    city: String
    postalCode: String
    email: String
  }

  type Query {
    userById(id: String!): User
  }

  type Mutation {
    updateUser(id: String!, userInput: UserInput!): User
  }
`;
