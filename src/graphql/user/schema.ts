import { makeExecutableSchema } from '@graphql-tools/schema';
import updateUser from './resolvers/updateUser';
import userById from './resolvers/userById';
import typedefs from './typedefs';

const userSchema = makeExecutableSchema({
  typeDefs: [typedefs],
  resolvers: {
    Query: {
      userById,
    },
    Mutation: {
      updateUser,
    },
  },
});

export default userSchema;
