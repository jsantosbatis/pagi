import { stitchSchemas } from '@graphql-tools/stitch';
import appSchema from './app/schema';
import payconiqSchema from './payconiq/schema';
import paypalSchema from './paypal/schema';
import stripeSchema from './stripe/schema';
import transactionSchema from './transaction/schema';
import userSchema from './user/schema';

export default stitchSchemas({
  subschemas: [
    userSchema,
    appSchema,
    transactionSchema,
    stripeSchema,
    payconiqSchema,
    paypalSchema,
  ],
});
