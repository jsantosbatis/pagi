import { omit } from 'lodash';
import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import StaticQrCode from '~/db/schemas/StaticQrCode';
import { Payconiq } from '~/libraries/payconiq';
import { Product } from '~/types/Product';
import { PayconiqConfig } from '~/types/ProviderConfig';
import { StaticQRCode } from '~/types/StaticQRCode';
import getPayconiqQRCode from './getPayconiqQRCode';

type TransactionInput = {
  amount: number;
  description: string;
  customerReference: string;
  product: Product;
};

export default async function updatePayconiqQRCode(
  id: string,
  transaction: TransactionInput,
): Promise<StaticQRCode | null> {
  const originQRCode = await getPayconiqQRCode(id);
  if (!originQRCode) {
    throw new Error('QRCode not found');
  }
  const app = await getApp(originQRCode.appId);

  const configs = await getProviderConfigsByAppId(originQRCode.appId);
  const payconiqConfig = configs?.find(
    (item) => item.type === 'Payconiq',
  ) as PayconiqConfig;

  const payconiq = new Payconiq({
    ...payconiqConfig,
    isProduction: app ? app.isProduction : false,
  });

  const qrCodeUrl = payconiq.getStaticQRCode({
    ...transaction,
    reference: id,
    paymentProfilId: payconiqConfig.paymentProfilId,
  });

  const response = await StaticQrCode.update(
    {
      PK: `static_qr_code_${id}`,
      SK: `static_qr_code`,
    },
    {
      ...omit(originQRCode, [
        'createdAt',
        'updatedAt',
        'SK',
        'PK',
        'GSI1SK',
        'GSI1PK',
        'GSI3PK',
      ]),
      ...transaction,
      product: JSON.stringify(transaction.product),
      qrCodeUrl,
    },
  );
  return {
    ...response,
    product: JSON.parse(response.product),
  };
}
