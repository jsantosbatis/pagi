import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import getTransaction from 'src/graphql/transaction/helpers/getTransaction';
import updateTransaction from 'src/graphql/transaction/helpers/updateTransaction';
import { getPayconiqCallbackUrl, getPayconiqCompleteUrl } from 'src/util';
import { CreatePayconiqPaymentResponse, Payconiq } from '~/libraries/payconiq';
import { PayconiqConfig } from '~/types/ProviderConfig';

export default async function createPayment(
  transactionId: string,
): Promise<CreatePayconiqPaymentResponse | null> {
  const originTransaction = await getTransaction(transactionId);
  if (!originTransaction) {
    throw new Error('Transaction not found');
  }
  const configs = await getProviderConfigsByAppId(originTransaction.appId);
  const payconiqConfig = configs?.find(
    (item) => item.type === 'Payconiq',
  ) as PayconiqConfig;
  const app = await getApp(originTransaction.appId);
  const payconiq = new Payconiq({
    ...payconiqConfig,
    isProduction: app ? app.isProduction : false,
  });

  const response = await payconiq.createPayment({
    amount: originTransaction.amount,
    description: originTransaction.description,
    reference: transactionId,
    callbackUrl: getPayconiqCallbackUrl(),
    returnUrl: getPayconiqCompleteUrl(transactionId),
  });

  await updateTransaction(originTransaction.id, {
    ...originTransaction,
    providerReference: response.paymentId,
    GSI2PK: response.paymentId,
  });
  return response;
}
