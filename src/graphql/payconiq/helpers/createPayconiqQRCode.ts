import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import StaticQrCode from '~/db/schemas/StaticQrCode';
import { Payconiq } from '~/libraries/payconiq';
import { Product } from '~/types/Product';
import { PayconiqConfig } from '~/types/ProviderConfig';
import { StaticQRCode } from '~/types/StaticQRCode';
import { generateId } from '~/util';

type TransactionInput = {
  amount: number;
  description: string;
  customerReference: string;
  product: Product;
};

export default async function createPayconiqQRCode(
  appId: string,
  transaction: TransactionInput,
): Promise<StaticQRCode | null> {
  const originApp = await getApp(appId);
  if (!originApp) {
    throw new Error('App not found');
  }
  const configs = await getProviderConfigsByAppId(originApp.id);
  const payconiqConfig = configs?.find(
    (item) => item.type === 'Payconiq',
  ) as PayconiqConfig;

  const payconiq = new Payconiq({
    ...payconiqConfig,
    isProduction: originApp ? originApp.isProduction : false,
  });

  const id = generateId();
  const qrCodeUrl = payconiq.getStaticQRCode({
    ...transaction,
    reference: id,
    paymentProfilId: payconiqConfig.paymentProfilId,
  });

  const response = await StaticQrCode.create({
    PK: `static_qr_code_${id}`,
    SK: `static_qr_code`,
    GSI1PK: `app_${appId}`,
    GSI1SK: `static_qr_code_${id}`,
    ...transaction,
    product: JSON.stringify(transaction.product),
    appId,
    qrCodeUrl,
    id,
  });

  return {
    ...response,
    product: JSON.parse(response.product),
  };
}
