import StaticQrCodeSchema from '~/db/schemas/StaticQrCode';
import { StaticQRCode } from '~/types/StaticQRCode';

export default async function getPayconiqQRCode(
  id: string,
): Promise<StaticQRCode | null> {
  const response = await StaticQrCodeSchema.query({
    PK: { eq: `static_qr_code_${id}` },
  }).exec();

  const [staticQRCode] = response;
  if (staticQRCode) {
    const parsed = staticQRCode.toJSON();
    return {
      ...parsed,
      product: JSON.parse(parsed.product),
    } as StaticQRCode;
  }
  return null;
}
