/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { StaticQRCode } from '~/types/StaticQRCode';
import { default as createPayconiqQRCodeHelper } from '../helpers/createPayconiqQRCode';

export default async function createStaticQRCode(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<StaticQRCode | null> {
  const { appId, transactionInput } = args;
  return createPayconiqQRCodeHelper(appId, transactionInput);
}
