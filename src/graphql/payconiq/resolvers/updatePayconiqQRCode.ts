/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { StaticQRCode } from '~/types/StaticQRCode';
import { default as updatePayconiqQRCodeHelper } from '../helpers/updatePayconiqQRCode';

export default async function updatePayconiqQRCode(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<StaticQRCode | null> {
  const { id, transactionInput } = args;
  return updatePayconiqQRCodeHelper(id, transactionInput);
}
