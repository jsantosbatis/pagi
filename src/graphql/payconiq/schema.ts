import { makeExecutableSchema } from '@graphql-tools/schema';
import commonTypeDefs from '../common/typedefs';
import createPayconiqQRCode from './resolvers/createPayconiqQRCode';
import createPayment from './resolvers/createPayment';
import updatePayconiqQRCode from './resolvers/updatePayconiqQRCode';
import typedefs from './typedefs';

const userSchema = makeExecutableSchema({
  typeDefs: [typedefs, commonTypeDefs],
  resolvers: {
    Query: {
      getSomething: (
        _parent: any,
        _args: any,
        _context: any,
        _info: any,
      ): boolean => {
        return true;
      },
    },
    Mutation: {
      createPayconiqPayment: createPayment,
      createPayconiqQRCode,
      updatePayconiqQRCode,
    },
  },
});

export default userSchema;
