import { gql } from 'apollo-server-micro';

export default gql`
  type Href {
    href: String
  }
  type Links {
    self: Href
    deeplink: Href
    qrcode: Href
    cancel: Href
    checkout: Href
  }
  type CreatePayconiqPaymentResponse {
    paymentId: String!
    links: Links
  }

  type Query {
    getSomething: Boolean
  }

  type StaticQRCode {
    id: ID
    appId: ID
    qrCodeUrl: String
    amount: Int
    description: String
    customerReference: String
    product: Product
  }

  input TransactionPayconiqInput {
    amount: Int
    description: String
    customerReference: String
    product: ProductInput!
  }

  type Mutation {
    createPayconiqPayment(transactionId: String!): CreatePayconiqPaymentResponse
    createPayconiqQRCode(
      appId: ID!
      transactionInput: TransactionPayconiqInput!
    ): StaticQRCode
    updatePayconiqQRCode(
      id: ID!
      transactionInput: TransactionPayconiqInput!
    ): StaticQRCode
  }
`;
