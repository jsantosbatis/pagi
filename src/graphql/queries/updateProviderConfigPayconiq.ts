import { DocumentNode, gql } from '@apollo/client';

export default function updateConfigPayconiq(): DocumentNode {
  return gql`
    mutation updateConfigPayconiq(
      $id: String!
      $providerConfigInput: ProviderConfigPayconiqInput!
    ) {
      updateProviderConfigPayconiq(
        id: $id
        providerConfigInput: $providerConfigInput
      ) {
        id
        apiKey
        paymentProfilId
        createdAt
        updatedAt
      }
    }
  `;
}
