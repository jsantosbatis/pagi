import { DocumentNode, gql } from '@apollo/client';

export default function createPayment(): DocumentNode {
  return gql`
    mutation createPayment($transactionId: String!) {
      createPayment(transactionId: $transactionId) {
        clientSecret
      }
    }
  `;
}
