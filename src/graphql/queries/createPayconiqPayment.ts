import { DocumentNode, gql } from '@apollo/client';

export default function createPayconiqPayment(): DocumentNode {
  return gql`
    mutation createPayconiqPayment($transactionId: String!) {
      createPayconiqPayment(transactionId: $transactionId) {
        paymentId
        links {
          self {
            href
          }
          deeplink {
            href
          }
          qrcode {
            href
          }
          checkout {
            href
          }
        }
      }
    }
  `;
}
