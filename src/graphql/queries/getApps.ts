import { DocumentNode, gql } from '@apollo/client';

export default function getApps(): DocumentNode {
  return gql`
    query apps {
      apps {
        id
        name
        isProduction
        returnUrl
        image
        createdAt
        updatedAt
      }
    }
  `;
}
