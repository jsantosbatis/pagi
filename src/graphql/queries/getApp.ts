import { DocumentNode, gql } from '@apollo/client';

export default function getApp(id: string): DocumentNode {
  return gql`
    query appById {
        appById(id: "${id}") {
            id
            name
            isProduction
            callbackUrl
            returnUrl
            image
            createdAt
            updatedAt
            providerConfigs {
                ...on ProviderConfigPaypal{
                  id
                  type
                  clientId
                  clientSecret
                }
                ...on ProviderConfigStripe{
                  id
                  type
                  publicKey
                  secretKey
                }
                ...on ProviderConfigPayconiq {
                  id
                  type
                  apiKey
                  paymentProfilId
                }
            }
        }
    }
      `;
}
