import { DocumentNode, gql } from '@apollo/client';

export default function createBancontactPayment(): DocumentNode {
  return gql`
    mutation createBancontactPayment($lang: String!, $transactionId: String!) {
      createBancontactPayment(lang: $lang, transactionId: $transactionId) {
        clientSecret
      }
    }
  `;
}
