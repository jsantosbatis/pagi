import { DocumentNode, gql } from '@apollo/client';

export default function createCharge(): DocumentNode {
  return gql`
    mutation createCharge($transactionId: String!, $source: String!) {
      createCharge(transactionId: $transactionId, source: $source)
    }
  `;
}
