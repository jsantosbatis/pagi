import { DocumentNode, gql } from '@apollo/client';
export default function getTransactionHistory(): DocumentNode {
  return gql`
    query transactionHistory(
      $appId: ID!
      $startDate: Float!
      $endDate: Float!
    ) {
      transactionHistory(
        appId: $appId
        startDate: $startDate
        endDate: $endDate
      ) {
        id
        paymentServiceProvider
        paymentMethodType
        amount
        netAmount
        fee
        status
        description
        date
      }
    }
  `;
}
