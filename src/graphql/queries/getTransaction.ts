import { DocumentNode, gql } from '@apollo/client';
export default function getTransaction(): DocumentNode {
  return gql`
    query transactionById($id: ID!) {
      transactionById(id: $id) {
        id
        appId
        paymentServiceProvider
        customerReference
        providerReference
        description
        amount
        netAmount
        fee
        status
        succeededAt
        createdAt
        updatedAt
        providerConfigs {
          ... on ProviderConfigStripe {
            id
            type
            publicKey
            isActive
          }
          ... on ProviderConfigPaypal {
            id
            type
            isActive
          }
          ... on ProviderConfigPayconiq {
            id
            type
            isActive
          }
        }
        products {
          id
          name
          amount
          quantity
        }
        app {
          id
          name
          image
        }
      }
    }
  `;
}
