import { DocumentNode, gql } from '@apollo/client';

export default function updateConfigPaypal(): DocumentNode {
  return gql`
    mutation updateConfigPaypal(
      $id: String!
      $providerConfigInput: ProviderConfigPaypalInput!
    ) {
      updateProviderConfigPaypal(
        id: $id
        providerConfigInput: $providerConfigInput
      ) {
        id
        clientId
        clientSecret
        createdAt
        updatedAt
      }
    }
  `;
}
