import { DocumentNode, gql } from '@apollo/client';

export default function updateApp(): DocumentNode {
  return gql`
    mutation updateApp($id: String!, $appInput: AppInput!) {
      updateApp(id: $id, appInput: $appInput) {
        id
        name
        isProduction
        returnUrl
        callbackUrl
        image
        createdAt
        updatedAt
      }
    }
  `;
}
