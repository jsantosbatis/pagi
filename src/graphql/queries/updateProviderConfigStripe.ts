import { DocumentNode, gql } from '@apollo/client';

export default function updateConfigStripe(): DocumentNode {
  return gql`
    mutation updateConfigStripe(
      $id: String!
      $providerConfigInput: ProviderConfigStripeInput!
    ) {
      updateProviderConfigStripe(
        id: $id
        providerConfigInput: $providerConfigInput
      ) {
        id
        publicKey
        secretKey
        createdAt
        updatedAt
      }
    }
  `;
}
