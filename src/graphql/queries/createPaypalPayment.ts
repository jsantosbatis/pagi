import { DocumentNode, gql } from '@apollo/client';

export default function createPaypalPayment(): DocumentNode {
  return gql`
    mutation createPaypalPayment($transactionId: String!) {
      createPaypalPayment(transactionId: $transactionId) {
        id
        links {
          href
          method
          rel
        }
      }
    }
  `;
}
