import { DocumentNode, gql } from '@apollo/client';

export default function createApp(): DocumentNode {
  return gql`
    mutation createApp($appInput: AppInput!) {
      createApp(appInput: $appInput) {
        id
        name
        isProduction
        returnUrl
        callbackUrl
        image
        createdAt
        updatedAt
      }
    }
  `;
}
