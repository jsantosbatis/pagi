import AppSchema from '~/db/schemas/App';
import { App } from '~/types/App';

export default async function getAppsByUserId(
  userId: string,
): Promise<App[] | null> {
  const response = await AppSchema.query({
    PK: { eq: `user_${userId}` },
    SK: { beginsWith: 'app' },
  }).exec();
  return response.toJSON() as App[];
}
