import db from '~/db/client';
import { ProviderConfig } from '~/types/ProviderConfig';

export default async function createProviderConfig(
  appId: string,
  providerConfig: ProviderConfig,
): Promise<ProviderConfig | null> {
  const { id } = providerConfig;
  const response = await db.create({
    PK: `providerConfig_${id}`,
    SK: 'providerConfig',
    ...providerConfig,
    GSI1PK: `app_${appId}`,
    GSI1SK: `providerConfig_${id}`,
    appId,
  });

  if (response) {
    return {
      ...providerConfig,
      ...response.toJSON(),
    } as ProviderConfig;
  }
  return null;
}
