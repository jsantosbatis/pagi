import { compact } from 'lodash';
import db from '~/db/client';
import { ProviderConfig } from '~/types/ProviderConfig';

export default async function getProviderConfigsByAppId(
  appId: string,
): Promise<ProviderConfig[] | null> {
  const response = await db
    .query({
      GSI1PK: { eq: `app_${appId}` },
      GSI1SK: { beginsWith: 'providerConfig' },
    })
    .using('GSI1')
    .exec();

  const configs = response.toJSON() as ProviderConfig[];
  return compact(
    configs.map((config) => {
      if (config.type === 'Payconiq') {
        return {
          isActive: !!(config.paymentProfilId && config.apiKey),
          ...config,
        };
      } else if (config.type === 'Paypal') {
        return {
          isActive: !!(
            config.webhookId &&
            config.clientId &&
            config.clientSecret
          ),
          ...config,
        };
      } else if (config.type === 'Stripe') {
        return {
          isActive: !!(
            config.webhookId &&
            config.webhookKey &&
            config.publicKey &&
            config.secretKey
          ),
          ...config,
        };
      }
      return null;
    }),
  );
}
