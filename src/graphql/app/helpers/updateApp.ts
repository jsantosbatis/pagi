/* eslint-disable no-console */
import AWS from 'aws-sdk';
import AppSchema from '~/db/schemas/App';
import { App } from '~/types/App';
import getApp from './getApp';

export default async function updateApp(
  id: string,
  app: App,
): Promise<App | null> {
  const originApp = await getApp(id);
  if (!originApp) {
    throw new Error('App not found');
  }

  const response = await AppSchema.update(
    { PK: originApp.PK, SK: originApp.SK },
    { ...app },
  );

  if (originApp.image && originApp.image !== app.image) {
    const S3 = new AWS.S3();
    S3.deleteObject(
      {
        Bucket: process.env.PAGI_AWS_BUCKET!,
        Key: `public/${originApp.image}`,
      },
      (err) => {
        if (err) console.info('File not deleted', err, err.stack);
        // an error occurred
        else console.info('File successfull deleted');
      },
    );
  }

  if (response) {
    return {
      ...response.toJSON(),
    } as App;
  }
  return null;
}
