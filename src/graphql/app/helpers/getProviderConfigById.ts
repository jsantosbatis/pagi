import db from '~/db/client';
import { ProviderConfig } from '~/types/ProviderConfig';

export default async function getProviderConfigById(
  id: string,
): Promise<ProviderConfig | null> {
  const response = await db
    .query({
      PK: { eq: `providerConfig_${id}` },
      SK: { eq: 'providerConfig' },
    })
    .exec();
  const [providerConfig] = response;
  if (providerConfig) {
    const config = providerConfig.toJSON() as ProviderConfig;
    if (config.type === 'Payconiq') {
      return {
        isActive: !!(config.paymentProfilId && config.apiKey),
        ...config,
      };
    } else if (config.type === 'Paypal') {
      return {
        isActive: !!(
          config.webhookId &&
          config.clientId &&
          config.clientSecret
        ),
        ...config,
      };
    } else if (config.type === 'Stripe') {
      return {
        isActive: !!(
          config.webhookId &&
          config.webhookKey &&
          config.publicKey &&
          config.secretKey
        ),
        ...config,
      };
    }
  }
  return null;
}
