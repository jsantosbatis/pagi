import getUser from 'src/graphql/user/helpers/getUser';
import { v4 } from 'uuid';
import AppSchema from '~/db/schemas/App';
import { App } from '~/types/App';
import createProviderConfig from './createProviderConfig';

export default async function createApp(
  userId: string,
  app: App,
): Promise<App | null> {
  const originUser = await getUser(userId);
  if (!originUser) {
    throw new Error('User not found');
  }
  const id = v4();
  const response = await AppSchema.create({
    PK: `user_${userId}`,
    SK: `app_${id}`,
    GSI1PK: `app_${id}`,
    GSI1SK: `app`,
    ...app,
    id,
  });

  await createProviderConfig(id, {
    type: 'Paypal',
    id: v4(),
    clientId: '',
    clientSecret: '',
    webhookId: '',
    appId: id,
  });

  await createProviderConfig(id, {
    type: 'Payconiq',
    id: v4(),
    apiKey: '',
    paymentProfilId: '',
    appId: id,
  });

  await createProviderConfig(id, {
    type: 'Stripe',
    id: v4(),
    publicKey: '',
    secretKey: '',
    webhookKey: '',
    webhookId: '',
    appId: id,
  });

  if (response) {
    return {
      ...response.toJSON(),
    } as App;
  }
  return null;
}
