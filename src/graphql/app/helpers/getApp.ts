import AppSchema from '~/db/schemas/App';
import { App } from '~/types/App';

export default async function getApp(appId: string): Promise<App | null> {
  const response = await AppSchema.query({
    GSI1PK: { eq: `app_${appId}` },
    GSI1SK: { eq: 'app' },
  })
    .using('GSI1')
    .exec();

  const [app] = response;
  if (app) {
    return {
      ...app.toJSON(),
    } as App;
  }
  return null;
}
