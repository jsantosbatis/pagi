import ProviderConfigPayconiq from '~/db/schemas/ProviderConfigPayconiq';
import ProviderConfigPaypal from '~/db/schemas/ProviderConfigPaypal';
import ProviderConfigStripe from '~/db/schemas/ProviderConfigStripe';
import { Paypal } from '~/libraries/paypal';
import StripeApi from '~/libraries/stripeApi';
import {
  PaypalConfig,
  ProviderConfig,
  StripeConfig,
} from '~/types/ProviderConfig';
import getApp from './getApp';
import getProviderConfigById from './getProviderConfigById';

export default async function updateProviderConfig(
  id: string,
  providerConfig: ProviderConfig,
): Promise<ProviderConfig | null> {
  const configOrigin = await getProviderConfigById(id);
  if (!configOrigin) {
    throw new Error('Config not found');
  }
  let response = null;
  switch (providerConfig.type) {
    case 'Stripe': {
      response = await updateStripeConfig(
        providerConfig,
        configOrigin as StripeConfig,
      );
      break;
    }
    case 'Payconiq':
      response = await ProviderConfigPayconiq.update(
        { PK: `providerConfig_${id}`, SK: `providerConfig` },
        { ...providerConfig },
      );
      break;
    case 'Paypal':
      response = await updatePaypalConfig(
        providerConfig,
        configOrigin as PaypalConfig,
      );
      break;
    default:
      break;
  }
  if (response) {
    return {
      ...response.toJSON(),
    } as ProviderConfig;
  }

  return null;
}

async function updateStripeConfig(
  config: StripeConfig,
  configOrigin: StripeConfig,
): Promise<StripeConfig | null> {
  let response = null;
  const stripeServer = StripeApi(config);
  try {
    const webhooks = await stripeServer.webhookEndpoints.list();
    let webhook = webhooks.data.find(
      (item) => item.id === configOrigin.webhookId,
    );
    if (!webhook) {
      webhook = await stripeServer.webhookEndpoints.create({
        api_version: '2020-08-27',
        enabled_events: [
          'source.chargeable',
          'payment_intent.payment_failed',
          'payment_intent.succeeded',
        ],
        url: process.env.PAGI_STRIPE_CALLBACK_URL!,
      });
      response = await ProviderConfigStripe.update(
        { PK: `providerConfig_${configOrigin.id}`, SK: `providerConfig` },
        {
          ...config,
          webhookId: webhook.id,
          webhookKey: webhook.secret,
        },
      );
    } else {
      response = await ProviderConfigStripe.update(
        { PK: `providerConfig_${configOrigin.id}`, SK: `providerConfig` },
        {
          ...config,
        },
      );
    }
  } catch (error) {
    throw new Error(error.message);
  }
  return response;
}

async function updatePaypalConfig(
  config: PaypalConfig,
  configOrigin: PaypalConfig,
): Promise<StripeConfig | null> {
  let response = null;
  const app = await getApp(configOrigin.appId);
  const paypalInstance = new Paypal({
    ...config,
    isProduction: app ? app.isProduction : false,
  });

  try {
    const webhooks = await paypalInstance.getWebhooks();
    let webhook = webhooks.find((item) => item.id === configOrigin.webhookId);
    if (!webhook) {
      webhook = await paypalInstance.createWebhook(
        process.env.PAGI_PAYPAL_CALLBACK_URL!,
        [{ name: 'CHECKOUT.ORDER.APPROVED' }],
      );
      response = await ProviderConfigPaypal.update(
        { PK: `providerConfig_${configOrigin.id}`, SK: `providerConfig` },
        {
          ...config,
          webhookId: webhook.id,
        },
      );
    } else {
      response = await ProviderConfigPaypal.update(
        { PK: `providerConfig_${configOrigin.id}`, SK: `providerConfig` },
        {
          ...config,
        },
      );
    }
  } catch (error) {
    throw new Error('Invalid Paypal Api keys');
  }
  return response;
}
