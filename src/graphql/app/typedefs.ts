import { gql } from 'apollo-server-micro';

export default gql`
  type App {
    id: ID
    name: String
    isProduction: Boolean
    returnUrl: String
    callbackUrl: String
    image: String
    createdAt: String
    updatedAt: String
    providerConfigs: [ProviderConfig]
  }

  input ProviderConfigPaypalInput {
    id: ID
    clientId: String
    clientSecret: String
  }

  input ProviderConfigPayconiqInput {
    id: ID
    apiKey: String
    paymentProfilId: String
  }

  input ProviderConfigStripeInput {
    id: ID
    publicKey: String
    secretKey: String
  }

  input AppInput {
    name: String
    isProduction: Boolean
    returnUrl: String
    callbackUrl: String
    image: String
  }

  type Query {
    appById(id: String!): App
    apps: [App]
  }

  type Mutation {
    createApp(appInput: AppInput!): App
    updateApp(id: String!, appInput: AppInput!): App
    updateProviderConfigPayconiq(
      id: String!
      providerConfigInput: ProviderConfigPayconiqInput!
    ): ProviderConfigPayconiq
    updateProviderConfigPaypal(
      id: String!
      providerConfigInput: ProviderConfigPaypalInput!
    ): ProviderConfigPaypal
    updateProviderConfigStripe(
      id: String!
      providerConfigInput: ProviderConfigStripeInput!
    ): ProviderConfigStripe
  }
`;
