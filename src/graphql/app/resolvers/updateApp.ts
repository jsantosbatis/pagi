/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { App } from '~/types/App';
import { default as updateAppHelper } from '../helpers/updateApp';

export default async function updateApp(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<App | null> {
  const { id, appInput } = args;
  return updateAppHelper(id, appInput);
}
