/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { ProviderConfig } from '~/types/ProviderConfig';
import { default as updateProviderConfigHelper } from '../helpers/updateProviderConfig';

export default async function updateProviderConfig(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<ProviderConfig | null> {
  const { id, providerConfigInput } = args;
  let providerConfig = null;
  if (_info.fieldName.includes('Stripe')) {
    providerConfig = { ...providerConfigInput, type: 'Stripe' };
  } else if (_info.fieldName.includes('Paypal')) {
    providerConfig = { ...providerConfigInput, type: 'Paypal' };
  } else if (_info.fieldName.includes('Payconiq')) {
    providerConfig = { ...providerConfigInput, type: 'Payconiq' };
  } else {
    throw new Error('Unable to determine config provider type');
  }
  return updateProviderConfigHelper(id, { ...providerConfig });
}
