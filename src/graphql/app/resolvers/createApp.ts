/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { App } from '~/types/App';
import { default as createAppHelper } from '../helpers/createApp';

export default async function createApp(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<App | null> {
  const { appInput } = args;
  const { user } = context;
  return createAppHelper(user.id, appInput);
}
