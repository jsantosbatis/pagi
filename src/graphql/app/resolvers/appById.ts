/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { App } from '~/types/App';
import getApp from '../helpers/getApp';

export default async function appById(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<App | null> {
  const { id } = args;
  return getApp(id);
}
