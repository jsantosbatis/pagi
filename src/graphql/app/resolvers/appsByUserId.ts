/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { App } from '~/types/App';
import { PagiContext } from '~/types/PagiContext';
import getAppsByUserId from '../helpers/getAppsByUserId';

export default async function appsByUserId(
  _parent: any,
  _args: any,
  context: PagiContext,
  _info: any,
): Promise<App[] | null> {
  return getAppsByUserId(context.user.id);
}
