import { makeExecutableSchema } from '@graphql-tools/schema';
import { App } from '~/types/App';
import { ProviderConfig } from '~/types/ProviderConfig';
import commonTypeDefs from '../common/typedefs';
import getProviderConfigsByAppId from './helpers/getProviderConfigsByAppId';
import appById from './resolvers/appById';
import apps from './resolvers/appsByUserId';
import createApp from './resolvers/createApp';
import updateApp from './resolvers/updateApp';
import updateProviderConfig from './resolvers/updateProviderConfig';
import typedefs from './typedefs';

const userSchema = makeExecutableSchema({
  typeDefs: [typedefs, commonTypeDefs],
  resolvers: {
    Query: {
      appById,
      apps,
    },
    Mutation: {
      createApp,
      updateApp,
      updateProviderConfigPayconiq: updateProviderConfig,
      updateProviderConfigPaypal: updateProviderConfig,
      updateProviderConfigStripe: updateProviderConfig,
    },
    App: {
      providerConfigs: (app: App) => {
        if (app.id) {
          return getProviderConfigsByAppId(app.id);
        } else {
          return null;
        }
      },
    },
    ProviderConfig: {
      __resolveType(source: ProviderConfig) {
        switch (source.type) {
          case 'Payconiq':
            return 'ProviderConfigPayconiq';
          case 'Stripe':
            return 'ProviderConfigStripe';
          case 'Paypal':
            return 'ProviderConfigPaypal';
        }
      },
    },
  },
});

export default userSchema;
