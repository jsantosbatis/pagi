import paypal from '@paypal/checkout-server-sdk';
import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import getTransaction from 'src/graphql/transaction/helpers/getTransaction';
import updateTransaction from 'src/graphql/transaction/helpers/updateTransaction';
import { Paypal } from '~/libraries/paypal';
import { PaypalOrder } from '~/types/PaypalOrder';
import { PaypalConfig } from '~/types/ProviderConfig';
import { getPaypalCompleteUrl, intToDecimal } from '~/util';

export default async function createPayment(
  transactionId: string,
): Promise<PaypalOrder | null> {
  const originTransaction = await getTransaction(transactionId);
  if (!originTransaction) {
    throw new Error('Transaction not found');
  }
  const configs = await getProviderConfigsByAppId(originTransaction.appId);
  const paypalConfig = configs?.find(
    (item) => item.type === 'Paypal',
  ) as PaypalConfig;
  const app = await getApp(originTransaction.appId);

  const paypalInstance = new Paypal({
    ...paypalConfig,
    isProduction: app?.isProduction,
  });
  const request = new paypal.orders.OrdersCreateRequest();

  request.requestBody({
    intent: 'CAPTURE',
    application_context: {
      return_url: getPaypalCompleteUrl('succeeded'),
      cancel_url: getPaypalCompleteUrl('failed'),
      brand_name: app?.name,
    },
    purchase_units: [
      {
        amount: {
          currency_code: 'EUR',
          value: intToDecimal(originTransaction.amount),
          breakdown: {
            item_total: {
              currency_code: 'EUR',
              value: intToDecimal(originTransaction.amount),
            },
          },
        },
        items: originTransaction.products.map((product) => ({
          name: product.name,
          unit_amount: {
            currency_code: 'EUR',
            value: intToDecimal(product.amount),
          },
          quantity: product.quantity,
          metadata: {
            productId: product.id,
            transactionId: originTransaction.id,
          },
        })),
        description: originTransaction.description,
        soft_descriptor: originTransaction.description,
      },
    ],
  });
  const response = await paypalInstance.client.execute(request);

  await updateTransaction(originTransaction.id, {
    ...originTransaction,
    providerReference: response.result.id,
    GSI2PK: response.result.id,
  });
  return response.result;
}
