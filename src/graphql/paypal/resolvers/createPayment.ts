/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { PaypalOrder } from '~/types/PaypalOrder';
import { default as createPaymentHelper } from '../helpers/createPayment';

export default async function createPayment(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<PaypalOrder | null> {
  const { transactionId } = args;
  return createPaymentHelper(transactionId);
}
