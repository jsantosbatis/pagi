import { gql } from 'apollo-server-micro';

export default gql`
  type Links {
    href: String
    method: String
    rel: String
  }

  type PaypalOrder {
    id: String!
    links: [Links]
  }

  type Query {
    getSomething: Boolean
  }

  type Mutation {
    createPaypalPayment(transactionId: String!): PaypalOrder
  }
`;
