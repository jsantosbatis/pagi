import { gql } from 'apollo-server-micro';

export default gql`
  union ProviderConfig =
      ProviderConfigPaypal
    | ProviderConfigPayconiq
    | ProviderConfigStripe

  type ProviderConfigPaypal {
    id: ID
    type: String
    clientId: String
    clientSecret: String
    isActive: Boolean
    createdAt: String
    updatedAt: String
  }

  type ProviderConfigPayconiq {
    id: ID
    type: String
    apiKey: String
    paymentProfilId: String
    isActive: Boolean
    createdAt: String
    updatedAt: String
  }

  type ProviderConfigStripe {
    id: ID
    type: String
    publicKey: String
    secretKey: String
    isActive: Boolean
    createdAt: String
    updatedAt: String
  }

  input ProductInput {
    id: ID!
    name: String!
    amount: Int!
    quantity: Int!
  }

  type Product {
    id: ID!
    name: String!
    amount: Int!
    quantity: Int!
  }
`;
