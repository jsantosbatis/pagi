import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import getTransaction from 'src/graphql/transaction/helpers/getTransaction';
import updateTransaction from 'src/graphql/transaction/helpers/updateTransaction';
import StripeApi from '~/libraries/stripeApi';
import { ClientSecret } from '~/types/ClientSecret';
import { StripeConfig } from '~/types/ProviderConfig';

export default async function createBancontactPayment(
  transactionId: string,
  lang: string,
): Promise<ClientSecret | null> {
  const originTransaction = await getTransaction(transactionId);
  if (!originTransaction) {
    throw new Error('Transaction not found');
  }
  const configs = await getProviderConfigsByAppId(originTransaction.appId);
  const stripeConfig = configs?.find(
    (item) => item.type === 'Stripe',
  ) as StripeConfig;

  const stripe = StripeApi(stripeConfig);
  const intent = await stripe.paymentIntents.create({
    amount: originTransaction.amount,
    currency: 'eur',
    payment_method_types: ['bancontact'],
    description: originTransaction.description,
    payment_method_options: {
      bancontact: {
        preferred_language: lang as any,
      },
    },
  });
  await updateTransaction(originTransaction.id, {
    ...originTransaction,
    providerReference: intent.id,
    GSI2PK: intent.id,
  });
  return { clientSecret: intent.client_secret };
}
