import getApp from 'src/graphql/app/helpers/getApp';
import getProviderConfigsByAppId from 'src/graphql/app/helpers/getProviderConfigsByAppId';
import getTransaction from 'src/graphql/transaction/helpers/getTransaction';
import updateTransaction from 'src/graphql/transaction/helpers/updateTransaction';
import eventBridge from '~/libraries/eventBridge';
import StripeApi from '~/libraries/stripeApi';
import { PaymentStatus } from '~/types/PaymentStatus';
import { StripeConfig } from '~/types/ProviderConfig';

export default async function createChargeHelper(
  transactionId: string,
  source: string,
): Promise<boolean> {
  console.log('create charge');
  const originTransaction = await getTransaction(transactionId);
  if (!originTransaction) {
    throw new Error('Transaction not found');
  }
  const configs = await getProviderConfigsByAppId(originTransaction.appId);
  const stripeConfig = configs?.find(
    (item) => item.type === 'Stripe',
  ) as StripeConfig;

  const stripe = StripeApi(stripeConfig);
  const charge = await stripe.charges.create({
    amount: originTransaction.amount,
    currency: 'eur',
    source,
    description: originTransaction.description,
    metadata: {
      integration_check: 'accept_a_payment',
    },
  });
  if (charge) {
    console.log(charge);
    const app = await getApp(originTransaction.appId);
    const balance = await stripe.balanceTransactions.retrieve(
      charge.balance_transaction as string,
    );
    const transactionUpdated = await updateTransaction(originTransaction.id, {
      ...originTransaction,
      providerReference: charge.id,
      fee: balance.fee,
      netAmount: originTransaction.amount - balance.fee,
      paymentMethodType: charge.payment_method_details
        ? charge.payment_method_details.type
        : null,
      succeededAt: charge.created as any,
      status: PaymentStatus.SUCCEEDED,
    });
    if (transactionUpdated && app) {
      await eventBridge.putPaymentEvent(app.callbackUrl, transactionUpdated);
    }
    return true;
  }

  return false;
}
