import { gql } from 'apollo-server-micro';

export default gql`
  type ClientSecret {
    clientSecret: String!
  }

  type Query {
    getSomething: Boolean
  }

  type Mutation {
    createPayment(transactionId: String!): ClientSecret
    createBancontactPayment(lang: String!, transactionId: String!): ClientSecret
    createCharge(transactionId: String!, source: String!): Boolean
  }
`;
