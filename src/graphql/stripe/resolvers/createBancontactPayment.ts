/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { ClientSecret } from '~/types/ClientSecret';
import { default as createBancontactPaymentHelper } from '../helpers/createBancontactPayment';

export default async function createBancontactPayment(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<ClientSecret | null> {
  const { transactionId, lang } = args;
  return createBancontactPaymentHelper(transactionId, lang);
}
