/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { default as createChargeHelper } from '../helpers/createCharge';

export default async function createCharge(
  _parent: any,
  args: any,
  _context: any,
  _info: any,
): Promise<boolean> {
  const { transactionId, source } = args;
  return createChargeHelper(transactionId, source);
}
