import { makeExecutableSchema } from '@graphql-tools/schema';
import createBancontactPayment from './resolvers/createBancontactPayment';
import createCharge from './resolvers/createCharge';
import createPayment from './resolvers/createPayment';
import typedefs from './typedefs';

const userSchema = makeExecutableSchema({
  typeDefs: [typedefs],
  resolvers: {
    Query: {
      getSomething: (
        _parent: any,
        _args: any,
        _context: any,
        _info: any,
      ): boolean => {
        return true;
      },
    },
    Mutation: {
      createPayment,
      createBancontactPayment,
      createCharge,
    },
  },
});

export default userSchema;
