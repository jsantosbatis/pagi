import React from 'react';

export default React.createContext({
  stripePublicKey: '',
  providerConfigs: [],
});
