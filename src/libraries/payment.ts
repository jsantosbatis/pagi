import { CardElement } from '@stripe/react-stripe-js';
import createBancontactPayment from 'src/graphql/queries/createBancontactPayment';
import { default as createChargeQuery } from 'src/graphql/queries/createCharge';
import { default as createPayconiqPaymentQuery } from 'src/graphql/queries/createPayconiqPayment';
import { default as createPaymentQuery } from 'src/graphql/queries/createPayment';
import { default as createPaypalPaymentQuery } from 'src/graphql/queries/createPaypalPayment';
import apolloClient from '~/libraries/apolloClient';
import { PaypalLink } from '~/types/PaypalLink';
import { getPayconiqCompleteUrl, getStripeCompleteUrl } from '../util';

export async function createPayment(id: string): Promise<string | null> {
  try {
    const response = await apolloClient.mutate({
      mutation: createPaymentQuery(),
      variables: { transactionId: id },
    });

    return response.data.createPayment.clientSecret;
  } catch (error) {
    return null;
  }
}

export async function createCharge(id: string, source: string): Promise<any> {
  try {
    const response = await apolloClient.mutate({
      mutation: createChargeQuery(),
      variables: { transactionId: id, source },
    });
    return response.data.createCharge;
  } catch (error) {
    return null;
  }
}

export async function initiatePaymentWithBancontact({
  stripe,
  name,
  lang = 'en',
  id,
}: any): Promise<any> {
  // eslint-disable-next-line no-useless-catch
  try {
    const response = await apolloClient.mutate({
      mutation: createBancontactPayment(),
      variables: { lang, transactionId: id },
    });

    const clientSecret = response.data.createBancontactPayment.clientSecret;
    const { error } = await stripe.confirmBancontactPayment(clientSecret, {
      payment_method: {
        billing_details: {
          name: name,
        },
      },
      return_url: getStripeCompleteUrl(),
    });
    if (error) {
      return null;
    }
  } catch (error) {
    return null;
  }
}

export async function initiatePaymentWithCard({
  stripe,
  name,
  elements,
  id,
}: any): Promise<boolean> {
  if (!stripe || !elements) {
    return false;
  }
  const clientSecret = await createPayment(id);
  const { error } = await stripe.confirmCardPayment(clientSecret, {
    payment_method: {
      card: elements.getElement(CardElement),
      billing_details: {
        name: name,
      },
    },
  });

  if (error) {
    throw error;
  } else {
    window.location = getStripeCompleteUrl(id) as any;
    return true;
  }
}

export async function initiatePaymentWithPayconiq(id: string): Promise<void> {
  const response = await apolloClient.mutate({
    mutation: createPayconiqPaymentQuery(),
    variables: { transactionId: id },
  });
  if (isMobile()) {
    window.location.href = `${
      response.data.createPayconiqPayment.links.deeplink.href
    }?
      returnUrl=${getPayconiqCompleteUrl(id)}`;
  } else {
    window.location.href =
      response.data.createPayconiqPayment.links.checkout.href;
  }
}

export async function initiatePaymentWithPaypal(id: string): Promise<void> {
  const response = await apolloClient.mutate({
    mutation: createPaypalPaymentQuery(),
    variables: { transactionId: id },
  });
  const href = response.data.createPaypalPayment.links.find(
    (item: PaypalLink) => item.rel === 'approve',
  ).href;
  window.location.href = href;
}

export function isIOS(): boolean {
  if (/iphone|XBLWP7/i.test(navigator.userAgent.toLowerCase())) {
    return true;
  }
  return false;
}

export function isAndroid(): boolean {
  if (/android|XBLWP7/i.test(navigator.userAgent.toLowerCase())) {
    return true;
  }
  return false;
}

export function isMobile(): boolean {
  return isIOS() || isAndroid();
}
