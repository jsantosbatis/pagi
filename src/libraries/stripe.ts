import { loadStripe, Stripe } from '@stripe/stripe-js';
import { StripeConfig } from '~/types/ProviderConfig';
let stripePromise: Promise<Stripe | null>;
const getStripe = (
  stripeConfig: StripeConfig,
  lang: string,
): Promise<Stripe | null> => {
  if (!stripePromise) {
    stripePromise = loadStripe(stripeConfig.publicKey, {
      locale: lang ? (lang as any) : 'en',
    });
  }
  return stripePromise;
};

export default getStripe;
