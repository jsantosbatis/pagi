import { AWSError } from 'aws-sdk';
import EventBridge from 'aws-sdk/clients/eventbridge';
import { PromiseResult } from 'aws-sdk/lib/request';
import { Transaction } from '~/types/Transaction';

const eventBridge = new EventBridge({
  apiVersion: '2015-10-07',
  region: process.env.PAGI_AWS_REGION!,
  credentials: {
    accessKeyId: process.env.PAGI_AWS_ACCESS_KEY_ID!,
    secretAccessKey: process.env.PAGI_AWS_SECRET_ACCESS_KEY!,
  },
});

export default {
  putPaymentEvent: async (
    callbackUrl: string,
    transaction: Transaction,
  ): Promise<PromiseResult<EventBridge.PutEventsResponse, AWSError>> => {
    const params = {
      Entries: [
        {
          Detail: JSON.stringify({
            url: callbackUrl,
            transaction,
          }),
          DetailType: 'PaymentEvent',
          Source: process.env.PAGI_EVENT_BRIDGE_SOURCE,
        },
      ],
    };
    return await eventBridge.putEvents(params).promise();
  },
};
