import axios from 'axios';

const EXT_URL = 'https://api.ext.payconiq.com/';
const PROD_URL = 'https://api.payconiq.com/';

type PayconiqParams = {
  apiKey: string;
  isProduction: boolean;
  paymentProfilId?: string;
};

export type CreatePayconiqPaymentResponse = {
  paymentId: string;
  links: Array<any>;
};

type CreatePaymentParams = {
  description: string;
  reference: string;
  amount: number;
  callbackUrl: string;
  returnUrl: string;
};

export type PaymentDetail = {
  paymentId: string;
  reference: string;
  createdAt: Date;
  expireAt: Date;
  succeededAt: Date;
  currency: string;
  status: string;
  amount: number;
  transferAmount: number;
  tippingAmount: number;
  totalAmount: number;
  description: string;
};

type GetStaticQRCode = {
  amount: number;
  description: string;
  reference: string;
  imageSize?: string;
  imageFormat?: string;
  paymentProfilId: string;
};

const QR_CODE_URL =
  'https://portal.payconiq.com/qrcode?f={image_format}&s={image_size}&c={payload}';
const PAYLOAD_URL =
  'https://payconiq.com/t/1/{payment_profil_id}?D={description}&A={amount}&R={reference}';

export class Payconiq {
  private apiKey: string;
  private url: string;
  constructor({ apiKey, isProduction = false }: PayconiqParams) {
    this.apiKey = apiKey;
    this.url = isProduction ? PROD_URL : EXT_URL;
  }

  public async createPayment({
    description,
    reference,
    amount,
    callbackUrl,
    returnUrl,
  }: CreatePaymentParams): Promise<CreatePayconiqPaymentResponse> {
    const response = await axios.post(
      `${this.url}v3/payments`,
      {
        currency: 'EUR',
        description,
        reference,
        amount,
        callbackUrl,
        returnUrl,
      },
      {
        headers: {
          Authorization: `Bearer ${this.apiKey}`,
          'Content-Type': 'application/json',
        },
      },
    );

    return {
      paymentId: response.data.paymentId,
      links: response.data._links,
    };
  }

  public async getPaymentDetails(paymentId: string): Promise<PaymentDetail> {
    const response = await axios.get(`${this.url}v3/payments/${paymentId}`, {
      headers: {
        Authorization: `Bearer ${this.apiKey}`,
        'Content-Type': 'application/json',
      },
    });
    return response.data;
  }

  public getStaticQRCode({
    amount,
    description,
    reference,
    imageSize = 'L',
    imageFormat = 'PNG',
    paymentProfilId,
  }: GetStaticQRCode): string {
    let payloadUrl = PAYLOAD_URL.replace(
      '{payment_profil_id}',
      paymentProfilId,
    );
    payloadUrl = payloadUrl.replace('{amount}', `${amount}`);
    payloadUrl = payloadUrl.replace(
      '{description}',
      encodeURIComponent(description),
    );
    payloadUrl = payloadUrl.replace(
      '{reference}',
      encodeURIComponent(reference),
    );
    let qrcodeUrl = QR_CODE_URL.replace('{image_size}', imageSize);
    qrcodeUrl = qrcodeUrl.replace('{image_format}', imageFormat);
    return qrcodeUrl.replace('{payload}', encodeURIComponent(payloadUrl));
  }
}
