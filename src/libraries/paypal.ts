import paypal from '@paypal/checkout-server-sdk';
import axios from 'axios';
import { IncomingHttpHeaders } from 'node:http';

type PaypalParams = {
  clientId: string;
  clientSecret: string;
  isProduction?: boolean;
};

type EventType = {
  name: string;
  description?: string;
};

type WebhookLink = {
  href: string;
  rel: string;
  method: string;
};

type Webhook = {
  id: string;
  url: string;
  event_types: EventType[];
  links: WebhookLink[];
};

export class Paypal {
  public client: any;
  public clientId: string;
  public clientSecret: string;
  public url: string;
  constructor({ clientId, clientSecret, isProduction = false }: PaypalParams) {
    let environment: null;
    if (isProduction) {
      environment = new paypal.core.LiveEnvironment(clientId, clientSecret);
      this.url = 'https://api-m.paypal.com';
    } else {
      environment = new paypal.core.SandboxEnvironment(clientId, clientSecret);
      this.url = 'https://api-m.sandbox.paypal.com';
    }
    this.clientId = clientId;
    this.clientSecret = clientSecret;
    this.client = new paypal.core.PayPalHttpClient(environment);
  }

  public async getToken(): Promise<string> {
    const response = await axios.post(
      `${this.url}/v1/oauth2/token`,
      'grant_type=client_credentials',
      {
        auth: {
          username: this.clientId,
          password: this.clientSecret,
        },
      },
    );
    return response.data.access_token;
  }

  public async verifyWebhook(
    headers: IncomingHttpHeaders,
    webhookId: string,
    event: any,
  ): Promise<boolean> {
    const token = await this.getToken();
    const response = await axios.post(
      `${this.url}/v1/notifications/verify-webhook-signature`,
      {
        auth_algo: headers['paypal-auth-algo'],
        cert_url: headers['paypal-cert-url'],
        transmission_id: headers['paypal-transmission-id'],
        transmission_sig: headers['paypal-transmission-sig'],
        transmission_time: headers['paypal-transmission-time'],
        webhook_id: webhookId,
        webhook_event: event,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    return response.data.verification_status === 'SUCCESS';
  }

  public async getWebhooks(): Promise<Webhook[]> {
    const token = await this.getToken();
    const response = await axios.get(`${this.url}/v1/notifications/webhooks`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    return response.data.webhooks;
  }

  public async createWebhook(
    url: string,
    events: EventType[],
  ): Promise<Webhook> {
    const token = await this.getToken();
    const response = await axios.post(
      `${this.url}/v1/notifications/webhooks`,
      {
        url,
        event_types: events,
      },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      },
    );
    return response.data;
  }
}
