import Stripe from 'stripe';
import { StripeConfig } from '~/types/ProviderConfig';

const getStripe = (stripeConfig: StripeConfig): Stripe => {
  return new Stripe(stripeConfig.secretKey, {
    apiVersion: '2020-08-27',
  });
};

export default getStripe;
