import * as dynamoose from 'dynamoose';
import { NextApiRequest, NextApiResponse } from 'next';

const withDB = (handler: any) => {
  return async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
    dynamoose.aws.sdk.config.update({
      accessKeyId: process.env.PAGI_AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.PAGI_AWS_SECRET_ACCESS_KEY,
      region: process.env.PAGI_AWS_REGION,
    });
    /*const ddb = new dynamoose.aws.sdk.DynamoDB({
      accessKeyId: process.env.PAGI_AWS_ACCESS_KEY_ID,
      secretAccessKey: process.env.PAGI_AWS_SECRET_ACCESS_KEY,
      region: process.env.PAGI_AWS_REGION,
    });
    // Set DynamoDB instance to the Dynamoose DDB instance
    dynamoose.aws.ddb.set(ddb);*/
    return handler(req, res);
  };
};

export default withDB;
