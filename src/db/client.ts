import createModel from './createModel';
import Main from './schemas/Main';

export default createModel({
  ...Main,
});
