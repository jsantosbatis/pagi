import createModel from '../createModel';

export default createModel({
  type: {
    type: String,
    default: 'Stripe',
  },
  publicKey: {
    type: String,
  },
  secretKey: {
    type: String,
  },
  webhookKey: {
    type: String,
  },
  webhookId: {
    type: String,
  },
  appId: {
    type: String,
  },
});
