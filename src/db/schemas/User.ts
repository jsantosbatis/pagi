import createModel from '../createModel';

export default createModel({
  firstName: { type: String },
  lastName: { type: String },
  vatNumber: { type: String },
  street: { type: String },
  box: { type: String },
  city: { type: String },
  postalCode: { type: String },
  email: { type: String },
});
