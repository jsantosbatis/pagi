import createModel from '../createModel';

export default createModel({
  type: {
    type: String,
    default: 'Payconiq',
  },
  apiKey: {
    type: String,
  },
  paymentProfilId: {
    type: String,
  },
  appId: {
    type: String,
  },
});
