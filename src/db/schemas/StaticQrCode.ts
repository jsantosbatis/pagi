import createModel from '../createModel';

export default createModel({
  customerReference: { type: String },
  description: { type: String },
  amount: { type: Number },
  qrCodeUrl: { type: String },
  appId: { type: String },
  product: { type: String },
});
