export default {
  id: {
    type: String,
  },
  PK: {
    type: String,
    hashKey: true,
  },
  SK: {
    type: String,
    rangeKey: true,
  },
  GSI1PK: {
    type: String,
    index: {
      global: true,
      name: 'GSI1',
      rangeKey: 'GSI1SK',
    },
  },
  GSI1SK: {
    type: String,
  },
  GSI2PK: {
    type: String,
    index: {
      global: true,
      name: 'GSI2TransactionByProviderReference',
    },
  },
  GSI3PK: {
    type: String,
    index: {
      global: true,
      name: 'GSI3TransactionHistory',
      rangeKey: 'GSI3SKTransactionDate',
    },
  },
  GSI3SKTransactionDate: {
    type: Number,
  },
};
