import createModel from '../createModel';

export default createModel({
  appId: { type: String },
  paymentServiceProvider: { type: String },
  customerReference: { type: String },
  providerReference: { type: String },
  paymentMethodType: { type: String },
  description: { type: String },
  amount: { type: Number },
  netAmount: { type: Number },
  fee: { type: Number },
  succeededAt: {
    type: Date,
  },
  status: { type: Number },
  products: { type: String },
});
