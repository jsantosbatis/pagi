import createModel from '../createModel';

export default createModel({
  name: {
    type: String,
  },
  isProduction: {
    type: Boolean,
  },
  returnUrl: {
    type: String,
  },
  callbackUrl: {
    type: String,
  },
  image: {
    type: String,
  },
});
