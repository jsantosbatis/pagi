import createModel from '../createModel';

export default createModel({
  type: {
    type: String,
    default: 'Paypal',
  },
  clientId: {
    type: String,
  },
  clientSecret: {
    type: String,
  },
  webhookId: {
    type: String,
  },
  appId: {
    type: String,
  },
});
