import createModel from '../createModel';

export default createModel({
  paymentServiceProvider: { type: String },
  paymentMethodType: { type: String },
  amount: { type: Number },
  netAmount: { type: Number },
  fee: { type: Number },
  status: { type: Number },
  description: { type: String },
});
