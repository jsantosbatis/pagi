import * as dynamoose from 'dynamoose';
import Main from './schemas/Main';

export default function createModel(schemaOptions: any): any {
  const table = process.env.PAGI_TABLE ?? 'pagi-dev';
  const schema = new dynamoose.Schema(
    {
      ...Main,
      ...schemaOptions,
    },
    {
      saveUnknown: true,
      timestamps: true,
    },
  );

  dynamoose.aws.sdk.config.update({
    accessKeyId: process.env.PAGI_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.PAGI_AWS_SECRET_ACCESS_KEY,
    region: process.env.PAGI_AWS_REGION,
  });

  return dynamoose.model(table, schema, {
    create: false,
  });
}
