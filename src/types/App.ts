import { RecordWithDates } from './Record';

export type App = RecordWithDates & {
  id: string;
  name: string;
  isProduction: boolean;
  returnUrl: string;
  callbackUrl: string;
  image: string;
};
