export type ClientSecret = {
  clientSecret: string | null;
};
