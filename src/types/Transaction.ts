import { PaymentStatus } from './PaymentStatus';
import { Product } from './Product';
import { RecordWithDates } from './Record';

export type Transaction = RecordWithDates & {
  id: string;
  appId: string;
  paymentServiceProvider: string;
  customerReference: string;
  providerReference: string;
  paymentMethodType: string | null;
  description: string;
  amount: number;
  netAmount: number;
  fee: number;
  url: string;
  status: PaymentStatus;
  succeededAt: Date | null;
  products: Product[];
};
