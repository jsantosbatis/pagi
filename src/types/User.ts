import { RecordWithDates } from './Record';

export type User = RecordWithDates & {
  id: string;
  firstName: string;
  lastName: string;
  vatNumber: string;
  street: string;
  box: string;
  city: string;
  postalCode: string;
  email: string;
};
