export enum PaymentStatus {
  'PENDING',
  'FAILED',
  'SUCCEEDED',
}
