import { PaypalLink } from './PaypalLink';

export type PaypalOrder = {
  id: string;
  links: [PaypalLink];
  status: string;
};
