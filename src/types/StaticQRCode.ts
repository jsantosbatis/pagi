import { Product } from './Product';
import { RecordWithDates } from './Record';

export type StaticQRCode = RecordWithDates & {
  id: string;
  appId: string;
  customerReference: string;
  description: string;
  amount: number;
  qrCodeUrl: string;
  product: Product;
};
