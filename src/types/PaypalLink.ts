export type PaypalLink = {
  href: string;
  method: string;
  rel: string;
};
