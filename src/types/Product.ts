export type Product = {
  id: string;
  name: string;
  amount: number;
  quantity: number;
};
