import { Transaction } from './Transaction';

export type TransactionHistory = Omit<
  Transaction,
  | 'customerReference'
  | 'providerReference'
  | 'products'
  | 'appId'
  | 'succeededAt'
> & {
  date: Date;
};
