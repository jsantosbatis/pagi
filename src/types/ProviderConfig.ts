import { RecordWithDates } from './Record';

export type PaypalConfig = RecordWithDates & {
  type: 'Paypal';
  id: string;
  clientId: string;
  clientSecret: string;
  webhookId: string;
  appId: string;
};

export type PayconiqConfig = RecordWithDates & {
  type: 'Payconiq';
  id: string;
  apiKey: string;
  paymentProfilId: string;
  appId: string;
};

export type StripeConfig = RecordWithDates & {
  type: 'Stripe';
  id: string;
  publicKey: string;
  secretKey: string;
  webhookKey: string;
  webhookId: string;
  appId: string;
};

export type ProviderConfig = { isActive?: boolean } & (
  | PaypalConfig
  | PayconiqConfig
  | StripeConfig
);
