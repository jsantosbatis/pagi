export type RecordBase = {
  PK: string;
  SK: string;
  GSI1PK: string;
  GSI1SK: string;
  GSI2PK: string;
  GSI3PK: string;
  GSI3SKTransactionDate: string;
  createdAt: Date;
  updatedAt: Date;
};

export type Record = Partial<RecordBase>;

export type RecordWithDates = Pick<Record, 'createdAt' | 'updatedAt'> &
  Partial<RecordBase>;
