import { FunctionComponent } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

type SidebarNavlinkProps = {
  label: string;
  path: string;
};
const SidebarNavLink: FunctionComponent<SidebarNavlinkProps> = ({
  label,
  path,
  children,
}) => {
  const router = useRouter();
  const activeLink = (route: string) => {
    return router.asPath === route;
  };
  return (
    <Link href={path}>
      <a
        className={`${
          activeLink(path) ? 'bg-black bg-opacity-25 ' : 'hover:bg-primary-800'
        } rounded-md py-2 px-3 text-sm font-medium text-white`}
      >
        {children ? (
          <span
            className={`${
              activeLink(path)
                ? 'text-gray-300'
                : 'text-gray-400 group-hover:text-gray-300'
            } mr-3 h-6 w-6`}
          >
            {children}
          </span>
        ) : (
          ''
        )}
        <span className="capitalize">{label}</span>
      </a>
    </Link>
  );
};

export default SidebarNavLink;
