import { FunctionComponent, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import { sortBy } from 'lodash';
import CheckoutContext from '~/contexts/checkout-context';

export const PaymentMethodType = {
  PAYCONIQ: 1,
  BANCONTACT: 2,
  CARD: 3,
  PAYPAL: 4,
};

interface Props {
  onChange: (paymentMethodType: number) => void;
  checked: number | null;
  googlePayButton: any;
}

const PaymentMethodList: FunctionComponent<Props> = ({
  onChange,
  checked,
  googlePayButton,
}) => {
  const { t } = useTranslation();

  const { providerConfigs } = useContext(CheckoutContext);
  let paymentMethods = providerConfigs.flatMap((config: any) => {
    if (config.type === 'Payconiq' && config.isActive) {
      return {
        label: 'Payconiq',
        type: PaymentMethodType.PAYCONIQ,
        order: 0,
        render: () => {
          return (
            <img
              src="/logo/payconiq-vertical.svg"
              className="h-12 w-auto -mr-2"
            />
          );
        },
      };
    } else if (config.type === 'Paypal' && config.isActive) {
      return {
        label: 'PayPal',
        type: PaymentMethodType.PAYPAL,
        order: 3,
        render: () => {
          return (
            <img src="/logo/paypal-vertical.svg" className="h-10 w-auto mr-2" />
          );
        },
      };
    } else if (config.type === 'Stripe' && config.isActive) {
      return [
        {
          label: 'Bancontact',
          type: PaymentMethodType.BANCONTACT,
          order: 1,
          render: () => (
            <img src="/logo/bancontact-vertical.svg" className="h-10 w-auto" />
          ),
        },
        {
          label: t('checkout.credit-card'),
          type: PaymentMethodType.CARD,
          order: 2,
          render: () => {
            return (
              <div className="flex items-center space-x-1">
                <img src="/logo/visa.svg" className="h-4 w-auto" />
                <img src="/logo/mastercard.svg" className="h-10 w-auto" />
              </div>
            );
          },
        },
      ];
    }
  });

  paymentMethods = sortBy(paymentMethods, ['order']);

  return (
    <div className="shadow sm:rounded-md sm:overflow-hidden">
      <div className="bg-white py-6 px-4 sm:p-6 space-y-1">
        <div>
          <h2
            id="payment_details_heading"
            className="text-lg leading-6 font-medium text-gray-900"
          >
            {t('checkout.payment-method')}
          </h2>
        </div>
        <div className="space-y-2 pt-2">
          {googlePayButton ?? ''}
          {paymentMethods.map((item, index) => {
            if (item) {
              return (
                <PaymentMethodItem
                  key={index}
                  label={item.label}
                  type={item.type}
                  onChange={onChange}
                  checked={
                    checked
                      ? checked === item.type
                      : index === 0 && onChange(item.type)
                  }
                >
                  {item.render()}
                </PaymentMethodItem>
              );
            }
          })}
        </div>
      </div>
    </div>
  );
};

const PaymentMethodItem: FunctionComponent<any> = ({
  children,
  label,
  type,
  onChange,
  checked,
}) => {
  const classChecked = (checked: boolean) => {
    let classe = `relative border p-4 flex rounded-md`;
    if (checked) {
      classe = 'border-secondary-500 rounded-lg border-2 pointer-events-none';
    } else {
      classe = 'border border-gray-300';
    }
    return classe;
  };
  const id = `method_type_${v4()}`;

  return (
    <>
      <div className={`relative p-4 flex rounded-md ${classChecked(checked)}`}>
        <label htmlFor={id} className="flex items-center w-full cursor-pointer">
          <input
            id={id}
            name="privacy_setting"
            type="radio"
            className="focus:ring-secondary-500 h-4 w-4 text-secondary-600 border-gray-300 cursor-pointer mr-4"
            checked={checked}
            onChange={() => onChange(type)}
          />
          <div className="w-full flex items-center justify-between h-8">
            <span className="block text-sm font-medium">{label}</span>
            {children}
          </div>
        </label>
      </div>
    </>
  );
};

export default PaymentMethodList;
