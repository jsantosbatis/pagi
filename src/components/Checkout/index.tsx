/* eslint-disable no-console */
import { FunctionComponent, useState } from 'react';
import GooglePayButtonWrapper from '../GooglePayButtonWrapper';
import PaymentMethodList, { PaymentMethodType } from '../PaymentMethodList';
import PaymentResume from '../PaymentResume';
import { useTranslation } from 'react-i18next';

import {
  createCharge,
  initiatePaymentWithBancontact,
  initiatePaymentWithCard,
  initiatePaymentWithPayconiq,
  initiatePaymentWithPaypal,
} from '~/libraries/payment';
import { useElements, useStripe, CardElement } from '@stripe/react-stripe-js';
import { getStripeCompleteUrl } from 'src/util';
import { Transaction } from '~/types/Transaction';
import Spinner from '../Spinner';
import ErrorAlert from '../ErrorAlert';

const styleCard = {
  style: {
    base: {
      color: '#32325d',
      fontFamily:
        'ui-sans-serif,system-ui,-apple-system,"Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji"',
      fontSmoothing: 'antialiased',
      fontSize: '15px',
      '::placeholder': {
        color: '#6A717E',
      },
    },
    invalid: {
      color: '#fa755a',
      iconColor: '#fa755a',
    },
  },
};

interface Props {
  transaction: Transaction;
  lang: string;
}

const Checkout: FunctionComponent<Props> = ({ transaction, lang }) => {
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState<
    number | null
  >(null);
  const [errors, setErrors] = useState([]);
  const [isFormComplete, setIsFormComplete] = useState(false);
  const [inputName, setInputName] = useState('');
  const [showBankFields, setShowBankFields] = useState(false);
  const [showSpinner, setShowSpinner] = useState(false);

  const { t } = useTranslation();
  const stripe = useStripe();
  const elements = useElements();

  const onChangePaymentMethod = (paymentMethodType: number) => {
    setSelectedPaymentMethod(paymentMethodType);
    setErrors([]);
    showExtraBankInput(paymentMethodType);
  };

  const onChangeCard = (event: any) => {
    if (event.complete) {
      setErrors([]);
      setIsFormComplete(true);
    } else if (event.error) {
      setIsFormComplete(false);
      setErrors([event.error.message] as any);
    }
  };

  const onGooglePaySuccess: (params: { source: string }) => void = async ({
    source,
  }) => {
    await createCharge(transaction.id, source);
    window.location = getStripeCompleteUrl(transaction.id) as any;
  };

  const onGooglePayError = (message: string) => {
    console.log(message);
  };

  const onGooglePayCancel = () => {
    console.log('cancel');
  };

  const processCardPayment = async (id: string) => {
    try {
      await initiatePaymentWithCard({
        stripe,
        elements,
        name: inputName,
        id,
      });
      setShowSpinner(false);
    } catch (error: any) {
      setErrors([error.message] as any);
      setShowSpinner(false);
    }
  };

  const processPayment = async () => {
    setShowSpinner(true);
    const { id } = transaction;
    try {
      if (selectedPaymentMethod === PaymentMethodType.CARD) {
        await processCardPayment(id);
      } else if (selectedPaymentMethod === PaymentMethodType.BANCONTACT) {
        await initiatePaymentWithBancontact({
          name: inputName,
          stripe,
          id,
          lang,
        });
      } else if (selectedPaymentMethod === PaymentMethodType.PAYCONIQ) {
        await initiatePaymentWithPayconiq(id);
      } else if (selectedPaymentMethod === PaymentMethodType.PAYPAL) {
        await initiatePaymentWithPaypal(id);
      }
      setShowSpinner(false);
    } catch (error: any) {
      setErrors([error.message] as any);
      setShowSpinner(false);
    }
  };

  const showExtraBankInput = (paymentMethodType: number) => {
    setShowBankFields(
      paymentMethodType === PaymentMethodType.CARD ||
        paymentMethodType === PaymentMethodType.BANCONTACT,
    );
  };

  const isPaymentButtonDisabled = () => {
    switch (selectedPaymentMethod) {
      case PaymentMethodType.PAYCONIQ:
      case PaymentMethodType.PAYPAL:
        return false;
      case PaymentMethodType.BANCONTACT:
        return inputName.trim() === '';
      case PaymentMethodType.CARD:
        return (
          !stripe ||
          !isFormComplete ||
          errors.length > 0 ||
          inputName.trim() === ''
        );
      default:
        break;
    }
  };

  return (
    <>
      <main className="relative -mt-32 pb-5 ">
        <div className="max-w-md mx-auto px-4 sm:px-6 lg:px-8 space-y-5">
          <PaymentResume transaction={transaction} />
          <PaymentMethodList
            onChange={onChangePaymentMethod}
            checked={selectedPaymentMethod}
            googlePayButton={
              <GooglePayButtonWrapper
                amount={transaction.amount}
                onSuccess={onGooglePaySuccess}
                onError={onGooglePayError}
                onCancel={onGooglePayCancel}
              />
            }
          />

          {/* Payment info */}
          {showBankFields ? (
            <div className="shadow sm:rounded-md sm:overflow-hidden">
              <div className="bg-white py-6 px-4 sm:p-6 space-y-1">
                <div>
                  <h2
                    id="payment_details_heading"
                    className="text-lg leading-6 font-medium text-gray-900"
                  >
                    {t('checkout.payment-information')}
                  </h2>
                </div>
                {selectedPaymentMethod === PaymentMethodType.BANCONTACT ? (
                  <div className="pt-1">
                    <div className="relative">
                      <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                        <svg
                          className="h-6 w-6 text-gray-300"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                        >
                          <path
                            fillRule="evenodd"
                            d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                      <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder={t('checkout.card-holder')}
                        onChange={(e) => setInputName(e.target.value)}
                        className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm"
                        style={{
                          fontFamily: 'Segoe UI',
                          fontSize: '15px',
                          color: '#32325d',
                          paddingLeft: '2.7em',
                        }}
                      />
                    </div>
                  </div>
                ) : (
                  false
                )}
                {selectedPaymentMethod === PaymentMethodType.CARD ? (
                  <div className="space-y-2 pt-1">
                    <div className="relative">
                      <div className="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                        <svg
                          className="h-6 w-6 text-gray-300"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 20 20"
                          fill="currentColor"
                        >
                          <path
                            fillRule="evenodd"
                            d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z"
                            clipRule="evenodd"
                          />
                        </svg>
                      </div>
                      <input
                        type="text"
                        name="name"
                        id="name"
                        placeholder={t('checkout.card-holder')}
                        onChange={(e) => setInputName(e.target.value)}
                        className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm"
                        style={{
                          fontFamily: 'Segoe UI',
                          fontSize: '15px',
                          color: '#32325d',
                          paddingLeft: '2.7em',
                        }}
                      />
                    </div>
                    <CardElement options={styleCard} onChange={onChangeCard} />
                  </div>
                ) : (
                  false
                )}
              </div>
              <ErrorAlert errors={errors} />
            </div>
          ) : (
            false
          )}

          <button
            type="button"
            onClick={processPayment}
            disabled={isPaymentButtonDisabled()}
            className="inline-flex w-full items-center justify-center px-4 py-2 border border-transparent shadow-sm text-base font-medium rounded-md text-white bg-primary-600 hover:bg-primary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:bg-primary-300 disabled:cursor-not-allowed"
          >
            {t('checkout.proceed-payment')}
          </button>
        </div>
      </main>
      <Spinner show={showSpinner} />
    </>
  );
};

export default Checkout;
