/* eslint-disable no-console */
import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

const CheckoutSkeleton: FunctionComponent = () => {
  const { t } = useTranslation();
  return (
    <main className="relative -mt-32 pb-5 ">
      <div className="max-w-md mx-auto px-4 sm:px-6 lg:px-8 space-y-5">
        <div className="shadow sm:rounded-md sm:overflow-hidden divide-y divide-gray-200">
          <div className="bg-white py-6 px-4 sm:p-6 space-y-1">
            <div>
              <h2
                id="payment_details_heading"
                className="text-lg leading-6 font-medium text-gray-900"
              >
                {t('checkout.resume')}
              </h2>
            </div>
            {Array(3)
              .fill(null)
              .map((_item, index) => (
                <div
                  key={index}
                  className="flex justify-between text-gray-600 text-base leading-5 text-transparent"
                >
                  <dt className="leading-5 font-medium  animate-pulse bg-gray-100 rounded-md">
                    Product name
                  </dt>
                  <dd className="animate-pulse bg-gray-100 rounded-md">
                    Price
                  </dd>
                </div>
              ))}
          </div>
          <div className="py-4 px-4 sm:px-6 bg-white font-semibold text-lg w-full">
            <span className="text-transparent animate-pulse bg-gray-100 w-full rounded-md">
              Total
            </span>
          </div>
        </div>
        <div className="shadow sm:rounded-md sm:overflow-hidden divide-y divide-gray-200">
          <div className="bg-white py-6 px-4 sm:p-6 space-y-1">
            <div>
              <h2
                id="payment_details_heading"
                className="text-lg leading-6 font-medium text-gray-900 pb-2"
              >
                {t('checkout.payment-method')}
              </h2>
            </div>
            <button
              type="button"
              className="inline-flex w-full items-center opacity-75 animate-pulse justify-center px-4 py-2 text-transparent border border-transparent shadow-sm text-base font-medium rounded-md text-white bg-black hover:bg-primary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:bg-primary-300 disabled:cursor-not-allowed"
            >
              <span className="text-transparent">
                {t('checkout.proceed-payment')}
              </span>
            </button>
            <div className="relative py-2">
              <div className="absolute inset-0 flex items-center">
                <div className="w-full border-t border-gray-300"></div>
              </div>
              <div className="relative flex justify-center text-sm">
                <span className="px-2 bg-white text-gray-500 font-semibold">
                  {t('checkout.or-pay-with')}
                </span>
              </div>
            </div>
            {Array(3)
              .fill(null)
              .map((_item, index) => (
                <div
                  key={index}
                  className="relative p-4 flex rounded-md border border-gray-300"
                >
                  <label className="flex items-center w-full cursor-pointer">
                    <input
                      name="privacy_setting"
                      type="radio"
                      className="focus:ring-secondary-500 h-4 w-4 text-secondary-600 border-gray-300 cursor-pointer mr-4"
                    />
                    <div className="w-full flex items-center justify-between h-8">
                      <span className="block text-sm font-medium text-transparent animate-pulse rounded-md bg-gray-100">
                        Payment
                      </span>
                    </div>
                  </label>
                </div>
              ))}
          </div>
        </div>
        <button
          type="button"
          className="inline-flex w-full items-center animate-pulse justify-center px-4 py-2 text-transparent border border-transparent shadow-sm text-base font-medium rounded-md text-white bg-primary-600 hover:bg-primary-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:bg-primary-300 disabled:cursor-not-allowed"
        >
          <span className="text-transparent">
            {t('checkout.proceed-payment')}
          </span>
        </button>
      </div>
    </main>
  );
};

export default CheckoutSkeleton;
