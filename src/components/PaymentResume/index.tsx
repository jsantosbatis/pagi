import { FunctionComponent } from 'react';
import { intToDecimal } from 'src/util';
import { Transaction } from '~/types/Transaction';
import NumberFormat from 'react-number-format';
import { useTranslation } from 'react-i18next';

interface Props {
  transaction: Transaction;
}
const PaymentResume: FunctionComponent<Props> = ({ transaction }) => {
  const { t } = useTranslation();
  return (
    <div className="shadow sm:rounded-md sm:overflow-hidden divide-y divide-gray-200">
      <div className="bg-white py-6 px-4 sm:p-6 space-y-1">
        <div>
          <h2
            id="payment_details_heading"
            className="text-lg leading-6 font-medium text-gray-900"
          >
            {t('checkout.resume')}
          </h2>
        </div>
        {transaction.products.map((product) => (
          <div
            key={product.id}
            className="flex justify-between text-gray-600 text-base leading-5"
          >
            <dt className="leading-5 font-medium">{product.name}</dt>
            <dd>
              <NumberFormat
                value={intToDecimal(product.amount * product.quantity)}
                displayType={'text'}
                thousandSeparator={true}
                suffix={'€'}
              />
            </dd>
          </div>
        ))}
      </div>
      <div className="py-4 px-4 flex justify-between sm:px-6 bg-white font-semibold text-lg">
        <span>Total</span>
        <span>
          <NumberFormat
            value={intToDecimal(transaction.amount)}
            displayType={'text'}
            thousandSeparator={true}
            suffix={'€'}
            className="leading-5"
          />
        </span>
      </div>
    </div>
  );
};
export default PaymentResume;
