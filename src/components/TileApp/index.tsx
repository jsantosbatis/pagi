import { S3Image } from 'aws-amplify-react';
import Link from 'next/link';
import { FunctionComponent } from 'react';
import { App } from '~/types/App';

type TileAppProps = {
  app: App;
};
export const TileApp: FunctionComponent<TileAppProps> = ({
  app,
}: TileAppProps) => {
  return (
    <Link href={`/app/${app.id}/home`}>
      <a className="bg-white rounded-lg shadow px-5 py-6 sm:px-6 transition duration-100  ease-out border border-opacity-0 border-cool-gray-500 hover:bg-cool-gray-50 hover:border-opacity-50 cursor-pointer">
        <div className="flex items-center">
          {app.image ? (
            <S3Image
              imgKey={app.image}
              className="inline-block h-14 w-14 rounded-full"
            />
          ) : (
            <span className="inline-block h-14 w-14 rounded-full overflow-hidden bg-gray-100">
              <svg
                className="h-full w-full text-gray-300"
                fill="currentColor"
                viewBox="0 0 24 24"
              >
                <path d="M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z" />
              </svg>
            </span>
          )}

          <span className="ml-4">
            <span className="text-lg font-semibold text-gray-800">
              {app.name}
            </span>
            <br />
            {app.isProduction ? (
              <span className="uppercase inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-green-100 text-green-800">
                production
              </span>
            ) : (
              <span className="uppercase inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-yellow-100 text-yellow-800">
                testing
              </span>
            )}
          </span>
        </div>
      </a>
    </Link>
  );
};

export default TileApp;
