/* eslint-disable no-console */
import { FunctionComponent } from 'react';

const TableSkeleton: FunctionComponent = () => {
  return (
    <>
      <div className="flex flex-col ">
        <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
          <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
              <table className="min-w-full divide-y divide-gray-200 bg-white">
                <thead className="bg-gray-50">
                  <tr>
                    <th scope="col" className="px-6 py-3 text-left flex">
                      <span className="animate-pulse bg-gray-300 rounded-md text-transparent w-full">
                        text
                      </span>
                    </th>
                    <th scope="col" className="px-6 py-3 text-right">
                      <span className="animate-pulse bg-gray-300 rounded-md text-transparent w-full">
                        text
                      </span>
                    </th>
                    <th scope="col" className="px-6 py-3 text-right">
                      <span className="animate-pulse bg-gray-300 rounded-md text-transparent w-full">
                        text
                      </span>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {Array(5)
                    .fill(null)
                    .map((_, index: number) => (
                      <tr key={index}>
                        <td className="px-6 py-4 flex">
                          <span className="animate-pulse bg-gray-100 rounded-md text-transparent w-full">
                            text
                          </span>
                        </td>
                        <td className="px-6 py-4 text-right">
                          <span className="animate-pulse bg-gray-100 rounded-md text-transparent">
                            text
                          </span>
                        </td>
                        <td className="px-6 py-4 text-right ">
                          <span className="animate-pulse bg-gray-100 rounded-md text-transparent">
                            text
                          </span>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TableSkeleton;
