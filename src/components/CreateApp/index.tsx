import { FunctionComponent, useContext, useState } from 'react';
import { Transition } from '@headlessui/react';
import CreateAppContext from '../../contexts/createApp-context';
import { useForm } from 'react-hook-form';
import { useMutation } from '@apollo/client';
import { App } from '~/types/App';
import createApp from 'src/graphql/queries/createApp';
import { Storage } from 'aws-amplify';
import { v4 } from 'uuid';
import dynamic from 'next/dynamic';
import { DEFAULT_PREVIEW_IMAGE } from 'src/constant';

const CreateApp: FunctionComponent = () => {
  const { isCreateAppOpen, toggleCreateApp } = useContext(CreateAppContext);
  const [imagePreview, setImagePreview] = useState(DEFAULT_PREVIEW_IMAGE);
  const [addApp] = useMutation(createApp());
  const { register, handleSubmit, watch, setValue, getValues } = useForm({
    defaultValues: {
      image: null,
      name: null,
      returnUrl: null,
      callbackUrl: null,
      isProduction: true,
    },
  });

  const reader = new FileReader();
  const watchIsProduction = watch('isProduction', false);

  const previewImage = (file: File) => {
    if (!file) return;
    reader.onload = function () {
      setImagePreview(reader.result as string);
    };
    reader.readAsDataURL(file);
  };

  const removeImage = () => {
    setImagePreview(DEFAULT_PREVIEW_IMAGE);
    setValue('image', null);
  };

  const onChange = (e: any) => {
    if (e) {
      const [file] = e.target.files;
      previewImage(file);
    }
  };

  const onSubmit = async (data: App) => {
    try {
      if (data.image && data.image.length > 0 && data.image[0]) {
        const { image } = data;
        const { key } = (await Storage.put(v4(), image[0], {
          contentType: 'image/png',
        })) as any;
        data.image = key;
      } else {
        data.image = '';
      }
      await addApp({ variables: { appInput: data } });
      toggleCreateApp(false);
    } catch (e) {
      console.error(e);
    }
  };

  const cancel = () => {
    toggleCreateApp(false);
  };

  return (
    <>
      <Transition show={isCreateAppOpen}>
        <section
          className="fixed inset-0 overflow-hidden z-10"
          aria-labelledby="slide-over-title"
          role="dialog"
          aria-modal="true"
        >
          <div className="absolute inset-0 overflow-hidden">
            {/* Background overlay, show/hide based on slide-over state. */}

            <Transition.Child
              enter="ease-in-out duration-500"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in-out duration-500"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <div
                className="absolute inset-0 bg-gray-500 bg-opacity-75 transition-opacity"
                aria-hidden="true"
              ></div>
            </Transition.Child>
            <div className="absolute inset-y-0 right-0 pl-10 max-w-full flex sm:pl-16">
              <Transition.Child
                enter="transform transition ease-in-out duration-500 sm:duration-700"
                enterFrom="translate-x-full"
                enterTo="translate-x-0"
                leave="transform transition ease-in-out duration-500 sm:duration-700"
                leaveFrom="translate-x-0"
                leaveTo="translate-x-full"
              >
                <div className="max-w-2xl h-full">
                  <form
                    className="h-full flex flex-col bg-white shadow-xl overflow-y-scroll"
                    onSubmit={handleSubmit(onSubmit)}
                  >
                    <div className="flex-1">
                      {/* Header */}
                      <div className="px-4 py-6 bg-gray-50 sm:px-6">
                        <div className="flex items-start justify-between space-x-3">
                          <div className="space-y-1">
                            <h2
                              className="text-lg font-medium text-gray-900"
                              id="slide-over-title"
                            >
                              New app
                            </h2>
                            <p className="text-sm text-gray-500">
                              Get started by filling in the information below to
                              create your new app.
                            </p>
                          </div>
                          <div className="h-7 flex items-center">
                            <button
                              type="button"
                              className="bg-white rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-primary-500"
                              onClick={() => toggleCreateApp(false)}
                            >
                              <span className="sr-only">Close panel</span>
                              {/* Heroicon name: outline/x */}
                              <svg
                                className="h-6 w-6"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                                aria-hidden="true"
                              >
                                <path
                                  strokeLinecap="round"
                                  strokeLinejoin="round"
                                  strokeWidth={2}
                                  d="M6 18L18 6M6 6l12 12"
                                />
                              </svg>
                            </button>
                          </div>
                        </div>
                      </div>
                      {/* Divider container */}
                      <div className="py-6 space-y-6 sm:py-0 sm:space-y-0 sm:divide-y sm:divide-gray-200">
                        {/* App name */}
                        <div className="space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
                          <div>
                            <label
                              htmlFor="name"
                              className="block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2"
                            >
                              Name
                            </label>
                          </div>
                          <div className="sm:col-span-2">
                            <input
                              type="text"
                              name="name"
                              id="name"
                              className="block w-full shadow-sm sm:text-sm focus:ring-primary-500 focus:border-primary-500 border-gray-300 rounded-md"
                              ref={register({ required: true })}
                            />
                          </div>
                        </div>
                        {/* Is production */}
                        <div className="space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
                          <div>
                            <label
                              htmlFor="isProduction"
                              className="block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2"
                            >
                              Production environment
                            </label>
                          </div>
                          <div className="sm:col-span-2 pt-2">
                            <button
                              type="button"
                              className={`${
                                watchIsProduction === true
                                  ? 'bg-secondary-500'
                                  : 'bg-gray-200'
                              } relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500`}
                              onClick={() => {
                                if (getValues('isProduction') === true) {
                                  setValue('isProduction', false);
                                } else {
                                  setValue('isProduction', true);
                                }
                              }}
                              aria-pressed="false"
                            >
                              <span className="sr-only">
                                Production environment
                              </span>
                              <span
                                aria-hidden="true"
                                className={`${
                                  watchIsProduction === true
                                    ? 'translate-x-5'
                                    : 'translate-x-0'
                                } inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200`}
                              />
                            </button>
                            <input
                              id="isProduction"
                              type="checkbox"
                              name="isProduction"
                              ref={register()}
                              className="hidden"
                              hidden
                            />
                          </div>
                        </div>
                        {/* Return url*/}
                        <div className="space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
                          <div>
                            <label
                              htmlFor="returnUrl"
                              className="block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2"
                            >
                              Return url
                            </label>
                          </div>
                          <div className="sm:col-span-2">
                            <input
                              type="text"
                              name="returnUrl"
                              id="returnUrl"
                              className="block w-full shadow-sm sm:text-sm focus:ring-primary-500 focus:border-primary-500 border-gray-300 rounded-md"
                              ref={register({ required: true })}
                            />
                          </div>
                        </div>
                        {/* Callback url*/}
                        <div className="space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
                          <div>
                            <label
                              htmlFor="callbackUrl"
                              className="block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2"
                            >
                              Callback url
                            </label>
                          </div>
                          <div className="sm:col-span-2">
                            <input
                              type="text"
                              name="callbackUrl"
                              id="callbackUrl"
                              className="block w-full shadow-sm sm:text-sm focus:ring-primary-500 focus:border-primary-500 border-gray-300 rounded-md"
                              ref={register()}
                            />
                          </div>
                        </div>
                        {/* Image url*/}
                        <div className="space-y-1 px-4 sm:space-y-0 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6 sm:py-5">
                          <div>
                            <label
                              htmlFor="image"
                              className="block text-sm font-medium text-gray-900 sm:mt-px sm:pt-2"
                            >
                              Image
                            </label>
                          </div>
                          <div className="sm:col-span-2 flex items-center">
                            <img
                              className="inline-block h-12 w-12 rounded-full"
                              src={imagePreview}
                            />
                            <div className="ml-4 flex">
                              <div className="relative bg-white py-2 px-3 border border-blue-gray-300 rounded-md shadow-sm flex items-center cursor-pointer hover:bg-blue-gray-50 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-blue-gray-50 focus-within:ring-blue-500">
                                <label
                                  htmlFor="image"
                                  className="relative text-sm font-medium text-blue-gray-900 pointer-events-none"
                                >
                                  <span>Change</span>
                                  <span className="sr-only"> user photo</span>
                                </label>
                                <input
                                  id="image"
                                  name="image"
                                  type="file"
                                  ref={register()}
                                  onChange={onChange}
                                  accept="image/png"
                                  className="absolute inset-0 w-full h-full opacity-0 cursor-pointer border-gray-300 rounded-md"
                                />
                              </div>
                              <button
                                type="button"
                                onClick={() => removeImage()}
                                className="ml-3 bg-transparent py-2 px-3 border border-transparent rounded-md text-sm font-medium text-blue-gray-900 hover:text-blue-gray-700 focus:outline-none focus:border-blue-gray-300 focus:ring-2 focus:ring-offset-2 focus:ring-offset-blue-gray-50 focus:ring-blue-500"
                              >
                                Remove
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    {/* Action buttons */}
                    <div className="flex-shrink-0 px-4 border-t border-gray-200 py-5 sm:px-6">
                      <div className="space-x-3 flex justify-end">
                        <button
                          type="button"
                          className="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500"
                          onClick={cancel}
                        >
                          Cancel
                        </button>
                        <button
                          type="submit"
                          className="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-primary-700 hover:bg-primary-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500"
                        >
                          Create
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </Transition.Child>
            </div>
          </div>
        </section>
      </Transition>
    </>
  );
};

export default dynamic(() => Promise.resolve(CreateApp), {
  ssr: false,
});
