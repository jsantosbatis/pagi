import { FunctionComponent, ReactNode } from 'react';
import ProfileDropdown from '../ProfileDropdown';
import SidebarNavLink from '../SidebarNavLink';
import { useRouter } from 'next/router';

interface LayoutProps {
  header?: ReactNode;
  body: ReactNode;
}

const Layout: FunctionComponent<LayoutProps> = ({ header, body }) => {
  const router = useRouter();
  const { app_id } = router.query;

  return (
    <div className="bg-gray-100 min-h-screen">
      <div className="relative bg-primary-700 pb-32 overflow-hidden">
        {/* Menu open: "bg-primary-900", Menu closed: "bg-transparent" */}
        <nav className="bg-transparent relative z-10 border-b border-teal-500 border-opacity-25 lg:bg-transparent lg:border-none">
          <div className="max-w-7xl mx-auto px-2 sm:px-4 lg:px-8">
            <div className="relative h-16 flex items-center justify-between lg:border-b lg:border-primary-800">
              <div className="px-2 flex items-center lg:px-0">
                <div className="flex-shrink-0">
                  <img
                    className="block h-8 w-auto"
                    src="https://tailwindui.com/img/logos/workflow-mark-teal-400.svg"
                    alt="Workflow"
                  />
                </div>
                <div className="hidden lg:block lg:ml-6 lg:space-x-4">
                  <div className="flex">
                    {/* Current: "bg-black bg-opacity-25", Default: "hover:bg-primary-800" */}
                    <SidebarNavLink
                      path={`/app/${app_id}/home`}
                      label="Dashboard"
                    />
                    <SidebarNavLink
                      path={`/app/${app_id}/products`}
                      label="Products"
                    />
                    <SidebarNavLink
                      path={`/app/${app_id}/transactions`}
                      label="Transactions"
                    />
                    <SidebarNavLink
                      path={`/app/${app_id}/settings`}
                      label="Settings"
                    />
                  </div>
                </div>
              </div>
              <div className="flex-1 px-2 flex justify-center lg:ml-6 lg:justify-end">
                <div className="max-w-lg w-full lg:max-w-xs">
                  <label htmlFor="search" className="sr-only">
                    Search
                  </label>
                  <div className="relative text-primary-100 focus-within:text-gray-400">
                    <div className="pointer-events-none absolute inset-y-0 left-0 pl-3 flex items-center">
                      {/* Heroicon name: solid/search */}
                      <svg
                        className="flex-shrink-0 h-5 w-5"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20"
                        fill="currentColor"
                        aria-hidden="true"
                      >
                        <path
                          fillRule="evenodd"
                          d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                          clipRule="evenodd"
                        />
                      </svg>
                    </div>
                    <input
                      id="search"
                      name="search"
                      className="block w-full bg-primary-700 bg-opacity-50 py-2 pl-10 pr-3 border border-transparent rounded-md leading-5 placeholder-primary-100 focus:outline-none focus:bg-white focus:ring-white focus:border-white focus:placeholder-gray-500 focus:text-gray-900 sm:text-sm"
                      placeholder="Search"
                      type="search"
                    />
                  </div>
                </div>
              </div>
              <div className="flex lg:hidden">
                {/* Mobile menu button */}
                <button
                  type="button"
                  className="p-2 rounded-md inline-flex items-center justify-center text-primary-200 hover:text-white hover:bg-primary-800 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
                  aria-controls="mobile-menu"
                  aria-expanded="false"
                >
                  <span className="sr-only">Open main menu</span>
                  {/*
          Icon when menu is closed.

          Heroicon name: outline/menu

          Menu open: "hidden", Menu closed: "block"
        */}
                  <svg
                    className="block flex-shrink-0 h-6 w-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                  </svg>
                  {/*
          Icon when menu is open.

          Heroicon name: outline/x

          Menu open: "block", Menu closed: "hidden"
        */}
                  <svg
                    className="hidden flex-shrink-0 h-6 w-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M6 18L18 6M6 6l12 12"
                    />
                  </svg>
                </button>
              </div>
              <div className="hidden lg:block lg:ml-4">
                <div className="flex items-center">
                  <button className="flex-shrink-0 rounded-full p-1 text-primary-200 hover:bg-primary-800 hover:text-white focus:outline-none focus:bg-primary-900 focus:ring-2 focus:ring-offset-2 focus:ring-offset-primary-900 focus:ring-white">
                    <span className="sr-only">View notifications</span>
                    {/* Heroicon name: outline/bell */}
                    <svg
                      className="h-6 w-6"
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      aria-hidden="true"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth={2}
                        d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                      />
                    </svg>
                  </button>
                  {/* Profile dropdown */}
                  <div className="relative flex-shrink-0 ml-4">
                    <div className="ml-3 relative">
                      <ProfileDropdown />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Mobile menu, show/hide based on menu state. */}
          <div className="bg-primary-900 lg:hidden" id="mobile-menu">
            <div className="pt-2 pb-3 px-2 space-y-1">
              {/* Current: "bg-black bg-opacity-25", Default: "hover:bg-primary-800" */}
              <a
                href="#"
                className="bg-black bg-opacity-25 block rounded-md py-2 px-3 text-base font-medium text-white"
              >
                Dashboard
              </a>
              <a
                href="#"
                className="hover:bg-primary-800 block rounded-md py-2 px-3 text-base font-medium text-white"
              >
                Jobs
              </a>
              <a
                href="#"
                className="hover:bg-primary-800 block rounded-md py-2 px-3 text-base font-medium text-white"
              >
                Applicants
              </a>
              <a
                href="#"
                className="hover:bg-primary-800 block rounded-md py-2 px-3 text-base font-medium text-white"
              >
                Company
              </a>
            </div>
            <div className="pt-4 pb-3 border-t border-primary-800">
              <div className="flex items-center px-4">
                <div className="flex-shrink-0">
                  <img
                    className="rounded-full h-10 w-10"
                    src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-1.2.1&ixqx=wC2KD4eoMH&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=4&w=320&h=320&q=80"
                    alt="profile_image"
                  />
                </div>
                <div className="ml-3">
                  <div className="text-base font-medium text-white">
                    Debbie Lewis
                  </div>
                  <div className="text-sm font-medium text-primary-200">
                    debbielewis@example.com
                  </div>
                </div>
                <button className="ml-auto flex-shrink-0 rounded-full p-1 text-primary-200 hover:bg-primary-800 hover:text-white focus:outline-none focus:bg-primary-900 focus:ring-2 focus:ring-offset-2 focus:ring-offset-primary-900 focus:ring-white">
                  <span className="sr-only">View notifications</span>
                  {/* Heroicon name: outline/bell */}
                  <svg
                    className="h-6 w-6"
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    aria-hidden="true"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth={2}
                      d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                    />
                  </svg>
                </button>
              </div>
              <div className="mt-3 px-2">
                <a
                  href="#"
                  className="block rounded-md py-2 px-3 text-base font-medium text-primary-200 hover:text-white hover:bg-primary-800"
                >
                  Your Profile
                </a>
                <a
                  href="#"
                  className="block rounded-md py-2 px-3 text-base font-medium text-primary-200 hover:text-white hover:bg-primary-800"
                >
                  Settings
                </a>
                <a
                  href="#"
                  className="block rounded-md py-2 px-3 text-base font-medium text-primary-200 hover:text-white hover:bg-primary-800"
                >
                  Sign out
                </a>
              </div>
            </div>
          </div>
        </nav>
        {/* Menu open: "bottom-0", Menu closed: "inset-y-0" */}
        <div
          className="inset-y-0 absolute flex justify-center inset-x-0 left-1/2 transform -translate-x-1/2 w-full overflow-hidden lg:inset-y-0"
          aria-hidden="true"
        >
          <div className="flex-grow bg-primary-900 bg-opacity-75" />
          <svg
            className="flex-shrink-0"
            width={1750}
            height={308}
            viewBox="0 0 1750 308"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              opacity=".75"
              d="M1465.84 308L16.816 0H1750v308h-284.16z"
              fill="#075985"
            />
            <path
              opacity=".75"
              d="M1733.19 0L284.161 308H0V0h1733.19z"
              fill="#0c4a6e"
            />
          </svg>
          <div className="flex-grow bg-primary-800 bg-opacity-75" />
        </div>
        {header ? <header className="relative py-10">{header}</header> : ''}
      </div>
      {body ? <main className="relative -mt-32">{body}</main> : ''}
    </div>
  );
};

export default Layout;
