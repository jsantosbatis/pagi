/* eslint-disable no-console */
import { FunctionComponent, ReactNode } from 'react';
import { useTranslation } from 'react-i18next';
import { getBucketUrl } from '~/util';

interface Props {
  checkoutComponent: ReactNode;
  name?: string;
  image?: string;
}

const CheckoutWrapper: FunctionComponent<Props> = ({
  checkoutComponent,
  name,
  image,
}) => {
  const { t } = useTranslation();
  return (
    <div className="bg-gray-100 min-h-screen">
      <div className="relative bg-light-blue-700 pb-32 overflow-hidden">
        <div
          className="inset-y-0 absolute flex justify-center inset-x-0 left-1/2 transform -translate-x-1/2 w-full overflow-hidden lg:inset-y-0"
          aria-hidden="true"
        >
          <div className="flex-grow bg-light-blue-900 bg-opacity-75" />
          <svg
            className="flex-shrink-0"
            width={1750}
            height={308}
            viewBox="0 0 1750 308"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              opacity=".75"
              d="M1465.84 308L16.816 0H1750v308h-284.16z"
              fill="#075985"
            />
            <path
              opacity=".75"
              d="M1733.19 0L284.161 308H0V0h1733.19z"
              fill="#0c4a6e"
            />
          </svg>
          <div className="flex-grow bg-light-blue-800 bg-opacity-75" />
        </div>
        <header className="relative py-10">
          <div className="max-w-md mx-auto px-4 sm:px-6 lg:px-8">
            {name && image ? (
              <div className="flex items-center">
                <img
                  src={getBucketUrl(image)}
                  className="inline-block h-12 w-12 rounded-full"
                />

                <h1 className="ml-2 text-xl font-bold text-white">{name}</h1>
              </div>
            ) : (
              <h1 className="text-3xl font-bold text-white">
                {t('checkout.title')}
              </h1>
            )}
          </div>
        </header>
      </div>
      {checkoutComponent}
    </div>
  );
};

export default CheckoutWrapper;
