/* eslint-disable no-console */
import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

const CheckoutExpired: FunctionComponent = () => {
  const { t } = useTranslation();
  return (
    <main className="relative -mt-32 pb-5 ">
      <div className="max-w-md mx-auto px-4 sm:px-6 lg:px-8 space-y-5">
        <div className="shadow sm:rounded-md sm:overflow-hidden divide-y divide-gray-200">
          <div className="bg-white py-6 px-4 sm:p-6 ">
            <div className="flex flex-col justify-center items-center space-y-4">
              <h2 className="text-lg leading-6 font-medium text-gray-900">
                {t('checkout.as-expired')}
              </h2>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-20 w-20 text-red-300"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth={2}
                  d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"
                />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default CheckoutExpired;
