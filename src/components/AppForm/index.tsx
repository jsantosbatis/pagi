import { FunctionComponent, useEffect, useState } from 'react';
import { useForm, useWatch } from 'react-hook-form';
import dynamic from 'next/dynamic';
import { Storage } from 'aws-amplify';
import { App } from '~/types/App';
import { v4 } from 'uuid';
import { useMutation } from '@apollo/client';
import updateAppQuery from 'src/graphql/queries/updateApp';
import { S3Image } from 'aws-amplify-react';
import { DEFAULT_PREVIEW_IMAGE } from 'src/constant';
import { omit } from 'lodash';

interface Props {
  data: App;
}

const AppForm: FunctionComponent<Props> = ({ data }) => {
  const [originData, setOriginData] = useState<App | null>(null);
  const [image, setImage] = useState('');
  const [imagePreview, setImagePreview] = useState(DEFAULT_PREVIEW_IMAGE);
  const [canSubmit, setCanSubmit] = useState(false);
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    reset,
    getValues,
    formState: { isValid },
  } = useForm({
    mode: 'onBlur',
  });

  const formState = useWatch({
    control,
  });

  const [updateApp] = useMutation(updateAppQuery());

  useEffect(() => {
    if (formState && originData) {
      const enableSubmit =
        (originData.isProduction != formState.isProduction ||
          originData.name != formState.name ||
          originData.returnUrl != formState.returnUrl ||
          originData.callbackUrl != formState.callbackUrl ||
          originData.image != image) &&
        isValid;
      setCanSubmit(enableSubmit);
    }
  }, [formState]);

  useEffect(() => {
    if (data) {
      setOriginData(data);
      reset({
        name: data.name,
        isProduction: data.isProduction,
        returnUrl: data.returnUrl,
        callbackUrl: data.callbackUrl,
        id: data.id,
      });
      setImage(data.image);
    }
  }, [data]);

  const reader = new FileReader();
  const watchIsProduction = watch('isProduction', false);

  const onChange = (e: any) => {
    if (e) {
      const [file] = e.target.files;
      previewImage(file);
      setImage(file);
    }
  };

  const previewImage = (file: File) => {
    if (!file) return;
    reader.onload = function () {
      setImagePreview(reader.result as string);
    };
    reader.readAsDataURL(file);
  };

  const removeImage = () => {
    setImagePreview(DEFAULT_PREVIEW_IMAGE);
    setImage('');
  };

  const onSubmit = async (data: App) => {
    try {
      if (image && typeof image === 'object') {
        const { key } = (await Storage.put(v4(), image, {
          contentType: 'image/png',
        })) as any;
        data.image = key;
      } else {
        data.image = image; // Image has not changed
      }
      await updateApp({
        variables: {
          appInput: omit(data, ['id']),
          id: originData!.id,
        },
      });
    } catch (e) {
      console.error(e);
    }
  };

  const cancel = () => {
    if (originData) {
      reset({ ...originData });
      setImage(originData.image);
    }
  };

  return (
    <section aria-labelledby="payment_details_heading">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="shadow sm:rounded-md sm:overflow-hidden divide-y divide-gray-200">
          <div className="bg-white py-6 px-4 sm:p-6 ">
            <div>
              <h2
                id="payment_details_heading"
                className="text-lg leading-6 font-medium text-gray-900"
              >
                App details
              </h2>
              <p className="mt-1 text-sm text-gray-500">
                Update your billing information. Please note that updating your
                location could affect your tax rates.
              </p>
            </div>
            <div className="mt-6 grid grid-cols-4 gap-6">
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="name"
                  className="block text-sm font-medium text-gray-700"
                >
                  Name
                </label>
                <input
                  type="text"
                  name="name"
                  id="name"
                  ref={register()}
                  className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm"
                />
              </div>
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="image"
                  className="block text-sm font-medium text-gray-700"
                >
                  Image
                </label>
                <div className="mt-1 flex items-center">
                  {image && image === originData?.image ? (
                    <S3Image
                      imgKey={image}
                      className="inline-block h-12 w-12 rounded-full"
                    />
                  ) : (
                    <img
                      className="inline-block h-12 w-12 rounded-full"
                      src={imagePreview}
                    />
                  )}

                  <div className="ml-4 flex">
                    <div className="relative bg-white py-2 px-3 border border-blue-gray-300 rounded-md shadow-sm flex items-center cursor-pointer hover:bg-blue-gray-50 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-blue-gray-50 focus-within:ring-blue-500">
                      <label
                        htmlFor="image"
                        className="relative text-sm font-medium text-blue-gray-900 pointer-events-none"
                      >
                        <span>Change</span>
                        <span className="sr-only"> user photo</span>
                      </label>
                      <input
                        id="image"
                        name="image"
                        type="file"
                        accept="image/png"
                        onChange={onChange}
                        className="absolute inset-0 w-full h-full opacity-0 cursor-pointer border-gray-300 rounded-md"
                      />
                    </div>
                    <button
                      type="button"
                      onClick={() => removeImage()}
                      className="ml-3 bg-transparent py-2 px-3 border border-transparent rounded-md text-sm font-medium text-blue-gray-900 hover:text-blue-gray-700 focus:outline-none focus:border-blue-gray-300 focus:ring-2 focus:ring-offset-2 focus:ring-offset-blue-gray-50 focus:ring-blue-500"
                    >
                      Remove
                    </button>
                  </div>
                </div>
              </div>
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="returnUrl"
                  className="block text-sm font-medium text-gray-700"
                >
                  Return url
                </label>
                <input
                  type="text"
                  name="returnUrl"
                  id="returnUrl"
                  ref={register()}
                  className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm"
                />
              </div>
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="callbackUrl"
                  className="block text-sm font-medium text-gray-700"
                >
                  Callback url
                </label>
                <input
                  type="text"
                  name="callbackUrl"
                  id="callbackUrl"
                  ref={register()}
                  className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm"
                />
              </div>
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="email_address"
                  className="block text-sm font-medium text-gray-700"
                >
                  Prodution environment
                </label>
                <div className="mt-1">
                  <button
                    type="button"
                    className={`${
                      watchIsProduction === true
                        ? 'bg-secondary-500'
                        : 'bg-gray-200'
                    } relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500`}
                    onClick={() => {
                      if (getValues('isProduction') === true) {
                        setValue('isProduction', false, {
                          shouldDirty: true,
                          shouldValidate: true,
                        });
                      } else {
                        setValue('isProduction', true, {
                          shouldDirty: true,
                          shouldValidate: true,
                        });
                      }
                    }}
                    aria-pressed="false"
                  >
                    <span className="sr-only">Production environment</span>
                    <span
                      aria-hidden="true"
                      className={`${
                        watchIsProduction === true
                          ? 'translate-x-5'
                          : 'translate-x-0'
                      } inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200`}
                    />
                  </button>
                  <input
                    id="isProduction"
                    type="checkbox"
                    name="isProduction"
                    ref={register()}
                    className="hidden"
                    hidden
                  />
                </div>
              </div>
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="appId"
                  className="block text-sm font-medium text-gray-700"
                >
                  App Id
                </label>
                <input
                  type="text"
                  name="id"
                  id="id"
                  ref={register()}
                  readOnly={true}
                  className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm bg-gray-100"
                />
              </div>
            </div>
          </div>
          <div className="py-4 px-4 flex justify-end sm:px-6 bg-white ">
            <button
              type="button"
              onClick={() => cancel()}
              disabled={!canSubmit}
              className="bg-white border border-gray-300 rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:hidden"
            >
              Cancel
            </button>
            <button
              type="submit"
              disabled={!canSubmit}
              className="ml-5 bg-primary-700 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-primary-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:cursor-not-allowed disabled:opacity-20"
            >
              Save
            </button>
          </div>
        </div>
      </form>
    </section>
  );
};

export default dynamic(() => Promise.resolve(AppForm), {
  ssr: false,
});
