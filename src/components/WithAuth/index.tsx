import { ReactNode, useEffect, useState } from 'react';
import { Auth } from 'aws-amplify';
import { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
const Layout = dynamic(() => import('../Layout'), { ssr: false });

export default function WithAuth(
  WrappedComponent: React.ComponentType,
): ReactNode {
  return () => {
    // checks whether we are on client / browser or server.
    if (typeof window !== 'undefined') {
      const [isAuth, setIsAuth] = useState(false);
      const [isLoading, setIsLoading] = useState(true);
      const router = useRouter();

      const checkAuth = async () => {
        try {
          await Auth.currentAuthenticatedUser();
          setIsAuth(true);
        } catch (error) {
          setIsAuth(false);
          router.push('/login');
        } finally {
          setIsLoading(false);
        }
      };

      useEffect(() => {
        checkAuth();
      }, []);

      if (isAuth && !isLoading) {
        return <WrappedComponent />;
      } else {
        return (
          <Layout
            header={
              <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <h1 className="text-3xl font-bold text-white">{''}</h1>
              </div>
            }
            body={null}
          />
        );
      }
    }
    return null;
  };
}
