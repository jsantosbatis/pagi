import { FunctionComponent, useEffect, useState } from 'react';
import { useForm, useWatch } from 'react-hook-form';
import dynamic from 'next/dynamic';
import { App } from '~/types/App';
import { useMutation } from '@apollo/client';
import updateConfigStripeQuery from 'src/graphql/queries/updateProviderConfigStripe';
import { StripeConfig } from '~/types/ProviderConfig';
interface Props {
  data: StripeConfig;
}

const ConfigStripeForm: FunctionComponent<Props> = ({ data }) => {
  const [originData, setOriginData] = useState<StripeConfig | null>(null);
  const [canSubmit, setCanSubmit] = useState(false);
  const {
    register,
    handleSubmit,
    control,
    reset,
    formState: { isValid },
  } = useForm({ mode: 'onBlur' });

  const formState = useWatch({
    control,
  });

  const [updateConfig] = useMutation(updateConfigStripeQuery());
  useEffect(() => {
    if (data) {
      setOriginData(data);
      reset({
        publicKey: data.publicKey,
        secretKey: data.secretKey,
      });
    }
  }, [data]);

  useEffect(() => {
    if (formState && originData) {
      const enableSubmit =
        (originData.publicKey != formState.publicKey ||
          originData.secretKey != formState.secretKey) &&
        isValid;
      setCanSubmit(enableSubmit);
    }
  }, [formState]);

  const onSubmit = async (data: App) => {
    try {
      await updateConfig({
        variables: {
          providerConfigInput: data,
          id: originData!.id,
        },
      });
    } catch (e) {
      console.error(e);
    }
  };

  const cancel = () => {
    if (originData) {
      reset({ ...originData });
    }
  };

  return (
    <section aria-labelledby="payment_details_heading">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="shadow sm:rounded-md sm:overflow-hidden divide-y divide-gray-200">
          <div className="bg-white py-6 px-4 sm:p-6 ">
            <div>
              <h2
                id="payment_details_heading"
                className="text-lg leading-6 font-medium text-gray-900"
              >
                Stripe Api
              </h2>
              <p className="mt-1 text-sm text-gray-500">
                Update your Stripe Api to use this provider.
              </p>
            </div>
            <div className="mt-6 grid grid-cols-4 gap-6">
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="publicKey"
                  className="block text-sm font-medium text-gray-700"
                >
                  Public key
                </label>
                <input
                  type="text"
                  name="publicKey"
                  id="publicKey"
                  ref={register()}
                  className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm"
                />
              </div>
              <div className="col-span-4 sm:col-span-2">
                <label
                  htmlFor="secretKey"
                  className="block text-sm font-medium text-gray-700"
                >
                  Secret key
                </label>
                <input
                  type="text"
                  name="secretKey"
                  id="secretKey"
                  ref={register()}
                  className="mt-1 block w-full border rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-primary-500 focus:border-primary-500 border-gray-300 sm:text-sm"
                />
              </div>
            </div>
          </div>
          <div className="py-4 px-4 flex justify-end sm:px-6 bg-white ">
            <button
              type="button"
              onClick={() => cancel()}
              disabled={!canSubmit}
              className="bg-white border border-gray-300 rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:hidden"
            >
              Cancel
            </button>
            <button
              type="submit"
              disabled={!canSubmit}
              className="ml-5 bg-primary-700 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-primary-800 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 disabled:cursor-not-allowed disabled:opacity-20"
            >
              Save
            </button>
          </div>
        </div>
      </form>
    </section>
  );
};

export default dynamic(() => Promise.resolve(ConfigStripeForm), {
  ssr: false,
});
