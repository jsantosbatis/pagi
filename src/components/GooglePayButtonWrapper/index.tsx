import GooglePayButton, {
  ReadyToPayChangeResponse,
} from '@google-pay/button-react';
import React, { useState, FunctionComponent, useContext } from 'react';
import CheckoutContext from '~/contexts/checkout-context';
import { intToDecimal } from '~/util';
import { useTranslation } from 'react-i18next';

const tokenizationSpecification = {
  type: 'PAYMENT_GATEWAY',
  parameters: {
    gateway: 'stripe',
    'stripe:version': '2020-03-02',
    'stripe:publishableKey': '',
  },
};

const allowedCardNetworks = ['AMEX', 'DISCOVER', 'MASTERCARD', 'VISA'];

const allowedCardAuthMethods = ['PAN_ONLY', 'CRYPTOGRAM_3DS'];

const baseCardPaymentMethod = {
  type: 'CARD',
  parameters: {
    allowedAuthMethods: allowedCardAuthMethods,
    allowedCardNetworks: allowedCardNetworks,
  },
};

const cardPaymentMethod = Object.assign(
  { tokenizationSpecification: tokenizationSpecification },
  baseCardPaymentMethod,
);

const clientConfiguration = {
  apiVersion: 2,
  apiVersionMinor: 0,
};

interface Props {
  amount: number;
  onSuccess: (params: any) => void;
  onError: (errorMessage: string) => void;
  onCancel: () => void;
}
const GooglePayButtonWrapper: FunctionComponent<Props> = ({
  amount,
  onSuccess,
  onError,
  onCancel,
}) => {
  const [isReadyToPay, setIsReadyToPay] = useState(false);
  const { stripePublicKey } = useContext(CheckoutContext);
  cardPaymentMethod.tokenizationSpecification.parameters[
    'stripe:publishableKey'
  ] = stripePublicKey;
  const { t } = useTranslation();

  const getPaymentRequest = () => {
    const paymentDataRequest: any = Object.assign({}, clientConfiguration);
    paymentDataRequest.allowedPaymentMethods = [cardPaymentMethod];
    paymentDataRequest.merchantInfo = {
      merchantName: process.env.NEXT_PUBLIC_GOOGLE_MERCHANT_NAME,
      merchantId: process.env.NEXT_PUBLIC_GOOGLE_MERCHANT_ID,
    };
    paymentDataRequest.transactionInfo = {
      totalPriceStatus: 'FINAL',
      totalPrice: `${intToDecimal(amount)}`,
      currencyCode: 'EUR',
    };
    return paymentDataRequest;
  };

  const onLoadPaymentData = async (paymentRequest: any) => {
    const paymentToken =
      paymentRequest.paymentMethodData.tokenizationData.token;
    try {
      onSuccess({ source: JSON.parse(paymentToken).id });
    } catch (error) {
      onError(error.message);
    }
  };

  const onGooglePayError = (error: Error) => {
    onError(error.message);
  };

  const onReadyToPayChange: (result: ReadyToPayChangeResponse) => void = ({
    isButtonVisible,
    isReadyToPay,
  }) => {
    setIsReadyToPay(isButtonVisible && isReadyToPay);
  };

  return <></>;
  return (
    <>
      <GooglePayButton
        environment={
          process.env.NEXT_PUBLIC_ENV === 'production' ? 'PRODUCTION' : 'TEST'
        }
        paymentRequest={getPaymentRequest()}
        onLoadPaymentData={onLoadPaymentData}
        onError={onGooglePayError}
        onReadyToPayChange={onReadyToPayChange}
        onCancel={onCancel}
        buttonType="plain"
        buttonSizeMode="fill"
        className="w-full"
      />
      {isReadyToPay ? (
        <div className="relative py-2">
          <div className="absolute inset-0 flex items-center">
            <div className="w-full border-t border-gray-300"></div>
          </div>
          <div className="relative flex justify-center text-sm">
            <span className="px-2 bg-white text-gray-500 font-semibold">
              {t('checkout.or-pay-with')}
            </span>
          </div>
        </div>
      ) : (
        false
      )}
    </>
  );
};

export default GooglePayButtonWrapper;
